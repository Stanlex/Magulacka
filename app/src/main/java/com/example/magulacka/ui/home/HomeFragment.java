package com.example.magulacka.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.magulacka.MainActivity;
import com.example.magulacka.R;
import com.example.magulacka.Skill;
import com.example.magulacka.databinding.FragmentHomeBinding;

import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    MainActivity Act;
    Timer TextUpdater;
    
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Act=(MainActivity)getActivity();

        return root;
    }
    public void onPause() {
        super.onPause();
        //Clear all updaters!
        TextUpdater.cancel();
        TextUpdater=null;
    }

    public class UpdateTexts extends TimerTask {
        String Hodiny;
        String Minuty;
        String Sekundy;
        void SetTimeStrings(long EndTimeInSecs){
            long RemainingSecs=EndTimeInSecs-System.currentTimeMillis()/1000;
            Hodiny=Long.toString(RemainingSecs/3600);
            if (Hodiny.length()==1){Minuty="0"+Minuty;}
            Minuty=Long.toString((RemainingSecs/60)%60);
            if (Minuty.length()==1){Minuty="0"+Minuty;}
            Sekundy=Long.toString((RemainingSecs)%60);
            if (Sekundy.length()==1){Sekundy="0"+Sekundy;}
        }
        public void UpdateTextsJob(){
            PrepocitatQuickSpelly();
            //Always keep MagCounter up to date.
            ((TextView)Act.findViewById(R.id.MagCounter)).setText(Act.MojePostava.AktMg+" mg");
            if (Act.MojePostava.AktMg>Act.MojePostava.MaxMg){
                //OverBuff
                SetTimeStrings(Act.MojePostava.OverbuffStartTime/1000+Act.MojePostava.OverbuffDoba);
                ((TextView)Act.findViewById(R.id.MagMax)).setText("Přebití! "+Hodiny+":"+Minuty+":"+Sekundy);
            }else{
                //Not Overbuff
                ((TextView)Act.findViewById(R.id.MagMax)).setText("Max: "+Act.MojePostava.MaxMg+" mg");
            }
            if (Act.MojePostava.GloomLevel>0){
                //SeroTimer
                SetTimeStrings(Act.MojePostava.CDTStartTime/1000+Act.MojePostava.DobaGloom[Act.MojePostava.GloomLevel]);
                ((TextView)Act.findViewById(R.id.CounterInfo)).setText("Šero("+Act.MojePostava.GloomLevel+"):");
                ((TextView)Act.findViewById(R.id.RegenView)).setText(Minuty+":"+Sekundy);
            }else if(!Act.MojePostava.RegenEnabled){
                //Stopped Regen timer
                ((TextView)Act.findViewById(R.id.CounterInfo)).setText("Stopped: ");
                Minuty=Long.toString((Act.MojePostava.DobaGloom[0]/60)%60);
                if (Minuty.length()==1){Minuty="0"+Minuty;}
                Sekundy=Long.toString((Act.MojePostava.DobaGloom[0])%60);
                if (Sekundy.length()==1){Sekundy="0"+Sekundy;}
                ((TextView)Act.findViewById(R.id.RegenView)).setText(Minuty+":"+Sekundy);
            }else if (Act.MojePostava.AktMg>=Act.MojePostava.MaxMg){
                //Stopped Regen timer
                ((TextView)Act.findViewById(R.id.CounterInfo)).setText("Regen:");
                Minuty=Long.toString((Act.MojePostava.DobaGloom[0]/60)%60);
                if (Minuty.length()==1){Minuty="0"+Minuty;}
                Sekundy=Long.toString((Act.MojePostava.DobaGloom[0])%60);
                if (Sekundy.length()==1){Sekundy="0"+Sekundy;}
                ((TextView)Act.findViewById(R.id.RegenView)).setText(Minuty+":"+Sekundy);
            }else{
                //Moving RegenTimer
                SetTimeStrings(Act.MojePostava.CDTStartTime/1000+Act.MojePostava.DobaGloom[0]);
                ((TextView)Act.findViewById(R.id.CounterInfo)).setText("Regen:");
                ((TextView)Act.findViewById(R.id.RegenView)).setText(Minuty+":"+Sekundy);
            }
        }

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable()
            {
                public void run(){UpdateTextsJob();}
            });

        }
    }

    @Override
    public void onResume() {

        if (Act.MojePostava==null){
            Toast.makeText(getContext(), "Samí lidi, žádní Jiní. Vytvoř postavu, uživateli, a ulož ji. Pak ti dovolím přepnout z této obrazovky.", Toast.LENGTH_LONG).show();
            NavController Nav;
            Nav=NavHostFragment.findNavController(this);
            Nav.navigate(R.id.navigation_charedit);
            super.onResume();
            return;
        }
        //Není možné vytvořit postavu, aniž by se uložila i se seznamem skillů. Seznam skillů si drží aktuální CharEdit (přes import) nebo SkillFragment.
        refreshZobrazeniPostavy();
        //Buttons
        binding.getRoot().findViewById(R.id.DatEnergiiButton).setOnClickListener(view -> GiveMGDialog(view));
        binding.getRoot().findViewById(R.id.ZiskejEnergiiButton).setOnClickListener(view -> GetMGDialog(view));
        binding.getRoot().findViewById(R.id.SetMagButton).setOnClickListener(view -> SetMGDialog(view));
        binding.getRoot().findViewById(R.id.GloomUpButton).setOnClickListener(view -> clickGoUpALevel(view));
        binding.getRoot().findViewById(R.id.GloomDown).setOnClickListener(view -> clickGoDownALevel(view));
        binding.getRoot().findViewById(R.id.GloomDragUp).setOnClickListener(view -> clickDragUpALevel(view));

        ((CheckBox)binding.getRoot().findViewById(R.id.OhniskoCheckBox)).setChecked(Act.Ohnisko);
        ((CheckBox)binding.getRoot().findViewById(R.id.SoloCheckBox)).setChecked(Act.RitualSolo);
        ((CheckBox)binding.getRoot().findViewById(R.id.RouchoCheckBox)).setChecked(Act.Roucho);
        ((CheckBox)binding.getRoot().findViewById(R.id.ZridloCheckBox)).setChecked(Act.Zridlo);
        binding.getRoot().findViewById(R.id.OhniskoCheckBox).setOnClickListener(view -> onCheckboxClick(view));
        binding.getRoot().findViewById(R.id.SoloCheckBox).setOnClickListener(view -> onCheckboxClick(view));
        binding.getRoot().findViewById(R.id.RouchoCheckBox).setOnClickListener(view -> onCheckboxClick(view));
        binding.getRoot().findViewById(R.id.ZridloCheckBox).setOnClickListener(view -> onCheckboxClick(view));
        for (int i=1;i<7;i++) {
            binding.getRoot().findViewById(Act.QSButtonID(i)).setOnClickListener(view -> QSClick(view));
        }
        TextUpdater= new Timer();
        UpdateTexts NewTask = new UpdateTexts();
        TextUpdater.schedule(NewTask,1,1000);
        super.onResume();
    }

    public void onCheckboxClick(View v){
        switch (v.getId()){
            case R.id.OhniskoCheckBox:
                Act.Ohnisko=((CheckBox)v).isChecked();
                break;
            case R.id.SoloCheckBox:
                Act.RitualSolo=((CheckBox)v).isChecked();
                break;
            case R.id.RouchoCheckBox:
                Act.Roucho=((CheckBox)v).isChecked();
                break;
            case R.id.ZridloCheckBox:
                Act.Zridlo=((CheckBox)v).isChecked();
                break;
        }
        PrepocitatQuickSpelly();
    }
    @Override
    public void onStart() {

        if (Act.MojePostava==null){        //Kontrola prvního spuštění!
            Toast.makeText(getContext(), "Samí lidi, žádní Jiní. Vytvoř postavu, uživateli, a ulož ji. Pak ti dovolím přepnout z této obrazovky.", Toast.LENGTH_LONG).show();
            NavController Nav;
            Nav=NavHostFragment.findNavController(this);
            Nav.navigate(R.id.navigation_charedit);
            super.onStart();
            return;
        }
        refreshZobrazeniPostavy();
        super.onStart();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void SetMGDialog(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        builder.setTitle("Nastavit energii");
        // Set up the input
        input.setText("");
        // Specify the type of input expected
        input.setInputType(InputType.TYPE_CLASS_NUMBER );
        input.setText("");builder.setMessage("Kolik že máš tedy energie?");
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String vstup = input.getText().toString();
                if (vstup.isEmpty()){vstup="0";}
                Act.NastavMagy(Integer.parseInt(vstup));
                Act.MojePostava.PamatujSi("Energie nastavena na "+vstup+"Mg.");
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public void GetMGDialog(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        builder.setTitle("Získat energii");
        // Set up the input
        input.setText("");
        // Specify the type of input expected
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText("");builder.setMessage("Kolik energie získáváš?");
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String vstup = input.getText().toString();
                if (vstup.isEmpty()){vstup="0";}
                Act.NastavMagy(Act.MojePostava.AktMg + Integer.parseInt(vstup));
                Act.MojePostava.PamatujSi("Získávám "+vstup+"Mg.");

            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
    public void GiveMGDialog(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        builder.setTitle("Dát energii");
        // Specify the type of input expected
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText("");builder.setMessage("Kolik energie dáváš?");
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String vstup = input.getText().toString();
                if (vstup.isEmpty()){vstup="0";}
                Act.NastavMagy(Act.MojePostava.AktMg - Integer.parseInt(vstup));
                Act.MojePostava.PamatujSi("Odevzdávám "+vstup+"Mg.");
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void PokusOZmenuHladiny(int KamLezu, boolean Vtazen){ //VZDY O JEDNU HLADINU. Nejde jinak.
        if (Vtazen) {
            if (KamLezu==1) {
                Act.MojePostava.GloomLevel=1;
                Act.startSeroTimer(false);
                PrepocitatQuickSpelly();
                Act.MojePostava.PamatujSi("Vtažen do první hladiny");
                return;
            }
            else{
                Act.NastavMagy(Act.MojePostava.AktMg);
                Act.MojePostava.GloomLevel=2;
                Act.startSeroTimer(false);
                PrepocitatQuickSpelly();
                Act.MojePostava.PamatujSi("Vtažen do druhé hladiny");
                return;
            }
        }
        switch (KamLezu){
            case 0: //Jsem v první
                if (Act.MojePostava.AktMg>=10){
                    Act.MojePostava.GloomLevel=0;
                    Act.stopSeroTimer();
                    //Stopnul jsem Šerý časovač. Jsme v realitě... Co regen?
                    if (Act.MojePostava.AktMg<Act.MojePostava.MaxMg) {
                        Act.startRegenTimer(false);
                    }
                    Act.MojePostava.PamatujSi("Padám do reality.");
                }else{
                    Toast.makeText(getContext(), "Uvíznutí v první hladině!!!", Toast.LENGTH_LONG).show();
                }
                break;
            case 1:
                if (Act.MojePostava.GloomLevel==2){
                    if (Act.MojePostava.AktMg>=20){
                        Act.MojePostava.GloomLevel=1;
                        Act.startSeroTimer(false);
                        Act.MojePostava.PamatujSi("Padám do první hladiny.");
                    }else{
                        Toast.makeText(getContext(), "Uvíznutí v druhé hladině!!!", Toast.LENGTH_LONG).show();
                    }
                }else{ //Jsem v realitě
                    if (Act.MojePostava.AktMg>=Act.MojePostava.CenaGloom1Final()){
                        Act.NastavMagy(Act.MojePostava.AktMg-Act.MojePostava.CenaGloom1Final());
                        Act.MojePostava.GloomLevel=1;
                        Act.startSeroTimer(false);
                        Act.MojePostava.PamatujSi("Vstupuji do první hladiny");
                    }else{
                        Toast.makeText(getContext(), "Málo energie!", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case 2:
                if (Act.MojePostava.MaxHladina<2) {
                    Toast.makeText(getContext(), "Šero tě do další hladiny nepustí.", Toast.LENGTH_LONG).show();
                    break;
                }
                if (Act.MojePostava.AktMg>=Act.MojePostava.CenaGloom2Final()){
                    Act.NastavMagy(Act.MojePostava.AktMg-Act.MojePostava.CenaGloom2Final());
                    Act.MojePostava.GloomLevel=2;
                    Act.startSeroTimer(false);
                    Act.MojePostava.PamatujSi("Vstupuji do druhé hladiny");
                }else{
                    Toast.makeText(getContext(), "Málo energie!", Toast.LENGTH_LONG).show();
                }
                break;
        }
        PrepocitatQuickSpelly();
    }
    public void clickGoUpALevel(View v){
        switch (Act.MojePostava.GloomLevel){
            case 0:
                PokusOZmenuHladiny(1, false);
                break;
            case 1:
                PokusOZmenuHladiny(2, false);
                break;
            case 2:
                Toast.makeText(getContext(), "Tak hluboko do šera si Magulačka netroufá.", Toast.LENGTH_LONG).show();
                break;
        }
        PrepocitatQuickSpelly();
    }
    public void clickGoDownALevel(View v){
        switch (Act.MojePostava.GloomLevel){
            case 0:
                Toast.makeText(getContext(), "Reálnější než realita to už nebude.", Toast.LENGTH_LONG).show();
                break;
            case 1:
                PokusOZmenuHladiny(0, false);
                break;
            case 2:
                PokusOZmenuHladiny(1, false);
                break;
        }
        PrepocitatQuickSpelly();
    }
    public void clickDragUpALevel(View v){
        switch (Act.MojePostava.GloomLevel){
            case 0:
                PokusOZmenuHladiny(1, true);
                break;
            case 1:
                PokusOZmenuHladiny(2, true);
                break;
            case 2:
                Toast.makeText(getContext(), "Tak hluboko do šera si Magulačka netroufá.", Toast.LENGTH_LONG).show();
                break;
        }
        PrepocitatQuickSpelly();
    }

    public void PrepocitatQuickSpelly(){
        for (int i=0 ; i<6 ; i++)
        {
            Skill Find = FindSkill(Act.MojePostava.QuickSpellNames[i]);
            if (Find!=null){
                int Cena = Act.ZjistiRealCenu(Find);
                final String ButtonText=Act.MojePostava.QuickSpellNames[i]+" ( "+Cena+" )";
                ((Button)binding.getRoot().findViewById(FindQSButtonID(i))).setText(ButtonText);
            }
        }
    }


    public void QSClick(View v){
        switch (v.getId()){
            case R.id.QuickSpell1Button:
                if (!Act.MojePostava.QuickSpellNames[0].equals(""))
                Act.ZkusPouzitSkill(FindSkill(Act.MojePostava.QuickSpellNames[0]));
                break;
            case R.id.QuickSpell2Button:
                if (!Act.MojePostava.QuickSpellNames[1].equals(""))
                Act.ZkusPouzitSkill(FindSkill(Act.MojePostava.QuickSpellNames[1]));
                break;
            case R.id.QuickSpell3Button:
                if (!Act.MojePostava.QuickSpellNames[2].equals(""))
                    Act.ZkusPouzitSkill(FindSkill(Act.MojePostava.QuickSpellNames[2]));
                break;
            case R.id.QuickSpell4Button:
                if (!Act.MojePostava.QuickSpellNames[3].equals(""))
                    Act.ZkusPouzitSkill(FindSkill(Act.MojePostava.QuickSpellNames[3]));
                break;
            case R.id.QuickSpell5Button:
                if (!Act.MojePostava.QuickSpellNames[4].equals(""))
                    Act.ZkusPouzitSkill(FindSkill(Act.MojePostava.QuickSpellNames[4]));
                break;
            case R.id.QuickSpell6Button:
                if (!Act.MojePostava.QuickSpellNames[5].equals(""))
                    Act.ZkusPouzitSkill(FindSkill(Act.MojePostava.QuickSpellNames[5]));
                break;
        }
    }

    public Skill FindSkill(String Name){
        if (Name.isEmpty()){return null;}
        for (Skill S:Act.MojePostava.Skilly ) {
            if (S.Jmeno.equals(Name)){
                return S;
            }
        }
        return null;
    }

    public int FindQSButtonID(int i){
        switch (i){
            case 0:
                return R.id.QuickSpell1Button;
            case 1:
                return R.id.QuickSpell2Button;
            case 2:
                return R.id.QuickSpell3Button;
            case 3:
                return R.id.QuickSpell4Button;
            case 4:
                return R.id.QuickSpell5Button;
            case 5:
                return R.id.QuickSpell6Button;
            default:
                return 0;
        }
    }
    public void refreshZobrazeniPostavy(){
        ((TextView)Act.findViewById(R.id.textViewJmeno)).setText(Act.MojePostava.Jmeno);
        ((TextView)Act.findViewById(R.id.SpecTextView)).setText(Act.MojePostava.Specializace);
        ((TextView)Act.findViewById(R.id.KatTextView)).setText(Act.MojePostava.Kategorie + ". Kategorie");
    }
}