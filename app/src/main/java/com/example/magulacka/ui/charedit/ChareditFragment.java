package com.example.magulacka.ui.charedit;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.magulacka.MainActivity;
import com.example.magulacka.Postava;
import com.example.magulacka.R;
import com.example.magulacka.databinding.FragmentChareditBinding;

public class ChareditFragment extends Fragment implements View.OnClickListener {

    MainActivity Act;
    private FragmentChareditBinding binding;
    int DisplayGloom2=0;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentChareditBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        //Collapsing/expanding the categories
        root.findViewById(R.id.textViewActionElements).setOnClickListener(this);
        //Buttons
        root.findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                OverwriteChar(view);
            }
        });
        root.findViewById(R.id.buttonCanDoGloom2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                SwitchGloom2();
            }
        });
        root.findViewById(R.id.buttonExport).setOnClickListener(this);
        root.findViewById(R.id.buttonImport).setOnClickListener(this);
        Act=(MainActivity)getActivity();
        if (Act.MojePostava!=null) {
            DisplayGloom2=(Act.MojePostava.MaxHladina>1) ? 1 : 0;
            if (DisplayGloom2==0){
                root.findViewById(R.id.layoutGloom2).setVisibility(View.GONE);
                ((TextView)root.findViewById(R.id.textViewGloom2Decide)).setText("Nezvládá druhou");
            }else{
                root.findViewById(R.id.layoutGloom2).setVisibility(View.VISIBLE);
                ((TextView)root.findViewById(R.id.textViewGloom2Decide)).setText("Zvládá druhou");
            }
        }
        return root;
    }
    public void SwitchGloom2(){
        if (DisplayGloom2==1){
            DisplayGloom2=0;
            ((TextView)getActivity().findViewById(R.id.textViewGloom2Decide)).setText("Nezvládá druhou");
            getActivity().findViewById(R.id.layoutGloom2).setVisibility(View.GONE);
        }else{
            DisplayGloom2=1;
            ((TextView)getActivity().findViewById(R.id.textViewGloom2Decide)).setText("Zvládá druhou");
            getActivity().findViewById(R.id.layoutGloom2).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewActionElements:
                toggleVisibility(R.id.layoutActionElements);
                break;
            case R.id.textViewRitualElements:
                toggleVisibility(R.id.layoutRitualElements);
                break;
            case R.id.textViewGloom:
                toggleVisibility(R.id.layoutGloom);
                break;
            case R.id.textViewRealityMagic:
                toggleVisibility(R.id.layoutRealityMagic);
                break;
            case R.id.textViewBasic:
                toggleVisibility(R.id.layoutBasicInfo);
                break;
            case R.id.buttonExport:
                exportCharacter();
                break;
            case R.id.buttonImport:
                importCharacter();
                break;
        }
    }
    public void importCharacter(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Import");
        builder.setMessage("Máš již vytvořenou postavu? Zkopíruj ji sem. Přepíšu tu současnou (pokud nějaká existuje).");
        // Set up the input
        final LinearLayout Layout = new LinearLayout(getContext());
        Layout.setOrientation(LinearLayout.VERTICAL);
        final EditText PostavaInput = new EditText(getContext());
        PostavaInput.setHint("<POSTAVA>...");
        PostavaInput.setVerticalScrollBarEnabled(true);
        PostavaInput.setMaxLines(10);
        Layout.addView(PostavaInput);

        builder.setView(Layout);
        // Set up the buttons
        builder.setPositiveButton("Importovat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!(PostavaInput.getText().toString().isEmpty())){
                    //Zahoď starou, načti novou.
                    Postava LoadedChar = new Postava();
                    if(LoadedChar.LoadSelfXML(PostavaInput.getText().toString())) { //Pokud se úspěšně načteš
                        Act.WriteToFile("Postava.txt",MainActivity.convertDocumentToString(Act.MojePostava.SaveSelfXMLDoc()));
                        Act.MojePostava=LoadedChar;
                    }else{
                        Toast.makeText(getContext(),"Něco se nepovedlo. Chyba? Překlep v zadávaném textu?", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public void exportCharacter(){
        ClipboardManager myClipBoard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        if (Act.MojePostava==null){
            Toast.makeText(getContext(),"Nemám postavu. Jsem smutná Magulačka.", Toast.LENGTH_LONG).show();
            return;
        }
        String PostavaToClip = MainActivity.convertDocumentToString(Act.MojePostava.SaveSelfXMLDoc());
        ClipData clip = ClipData.newPlainText("", PostavaToClip );
        myClipBoard.setPrimaryClip(clip);
        Toast.makeText(getContext(),"Postava zkopírována do clipboardu. Je možné ji uložit.", Toast.LENGTH_LONG).show();
    }
    public void Rekni(String zprava){
        Toast.makeText(getContext(),zprava,Toast.LENGTH_LONG).show();
    }
    public void OverwriteChar(View v){
        Postava Char=new Postava();
        boolean FirstTime=false;
        if (Act.MojePostava==null){FirstTime=true;}
        String FoundData;
        FoundData=((EditText)Act.findViewById(R.id.editTextName)).getText().toString();
        if (!FoundData.isEmpty()) {Char.Jmeno= FoundData;} else {
            if (FirstTime){Rekni("Postava nemá jméno"); return;}else{Char.Jmeno=Act.MojePostava.Jmeno;}}
        FoundData=((EditText)Act.findViewById(R.id.editTextClass)).getText().toString();
        if (!FoundData.isEmpty()) {Char.Specializace= FoundData;} else {
            if (FirstTime){Rekni("Postava nemá specializaci"); return;}else{Char.Specializace=Act.MojePostava.Specializace;}}
        FoundData=((EditText)Act.findViewById(R.id.editTextCategory)).getText().toString();
        if (!FoundData.isEmpty()) {Char.Kategorie = parseInt(FoundData);} else {
            if (FirstTime){Rekni("Postava nemá kategorii? Nope. Nope! I'm outta here."); return;}else{Char.Kategorie=Act.MojePostava.Kategorie;}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRegenAmount)).getText().toString();
        if (!FoundData.isEmpty()) {Char.CenaGloom[0] = parseInt(FoundData);} else{
            if (FirstTime){Rekni("Postava neregeneruje magy? No, jak myslíš...");}else{Char.CenaGloom[0]=Act.MojePostava.CenaGloom[0];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRegenTime)).getText().toString();
        if (!FoundData.isEmpty()) {Char.DobaGloom[0] = parseInt(FoundData)*60;} else {
            if (FirstTime){Rekni("Chybí interval regenerace."); return;}else{Char.DobaGloom[0]=Act.MojePostava.DobaGloom[0];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextMaxMg)).getText().toString();
        if (!FoundData.isEmpty()) {Char.MaxMg = parseInt(FoundData); Char.AktMg=Char.MaxMg;} else {
            if (FirstTime){Rekni("Chybí maximum energie."); return;}else{Char.MaxMg=Act.MojePostava.MaxMg; Char.AktMg=Char.MaxMg;}}
        FoundData=((EditText)Act.findViewById(R.id.editTextOverbuffMultiplier)).getText().toString();
        FoundData.replace(',','.'); //Protože desetinná čárka.
        if (!FoundData.isEmpty()) {Char.OverbuffMultiplier = parseDouble(FoundData);} else {
            if (FirstTime){Rekni("Nebyl nastaven Overbuff... Tak není, no."); Char.OverbuffMultiplier=1;}else{Char.OverbuffMultiplier=Act.MojePostava.OverbuffMultiplier;}}
        FoundData=((EditText)Act.findViewById(R.id.editTextOverbuffLen)).getText().toString(); //BACHA PŘI ZOBRAZOVÁNÍ!!!
        if (!FoundData.isEmpty()) {Char.OverbuffDoba = parseInt(FoundData)*3600;} else {
            if (FirstTime){Rekni("Nebyl nastaven Overbuff... Tak není, no."); Char.OverbuffDoba=0;}else{Char.OverbuffDoba=Act.MojePostava.OverbuffDoba;}}
        FoundData=((EditText)Act.findViewById(R.id.editTextActionSleva)).getText().toString();
        if (!FoundData.isEmpty()) {Char.SlevaSpelly[0] = parseInt(FoundData);}else{
            if (!FirstTime){Char.SlevaSpelly[0]=Act.MojePostava.SlevaSpelly[0];}}
        //Práce s šerem
        FoundData=((EditText)Act.findViewById(R.id.editTextGloom1Price)).getText().toString();
        if (!FoundData.isEmpty()) {Char.CenaGloom[1] = parseInt(FoundData);} else {
            if (FirstTime){Rekni("Šero bude vysávat alespoň za 1mg. (Možná pokud někdy přidám CP mód, tohle omezení půjde obejít.) "); Char.CenaGloom[1]=0;}else{Char.CenaGloom[1]=Act.MojePostava.CenaGloom[1];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextGloom1Bonus)).getText().toString();
        if (!FoundData.isEmpty()) {Char.SlevaSpelly[1] = parseInt(FoundData);}else{if (!FirstTime){Char.SlevaSpelly[1]=Act.MojePostava.SlevaSpelly[1];}}
        if (DisplayGloom2==0){Char.MaxHladina=1;}
        if (DisplayGloom2==1){
            Char.MaxHladina=2;
            FoundData=((EditText)Act.findViewById(R.id.editTextGloom2Price)).getText().toString();
            if (!FoundData.isEmpty()) {Char.CenaGloom[2] = parseInt(FoundData);} else {
                if (FirstTime){Rekni("Druhé šero bude vysávat alespoň za 1mg. (Možná pokud někdy přidám CP mód, tohle omezení půjde obejít.) "); Char.CenaGloom[2]=0;}else{Char.CenaGloom[2]=Act.MojePostava.CenaGloom[2];}}
            FoundData=((EditText)Act.findViewById(R.id.editTextGloom2Bonus)).getText().toString();
            if (!FoundData.isEmpty()) {Char.SlevaSpelly[2] = parseInt(FoundData);}else{Char.SlevaSpelly[2]=Act.MojePostava.SlevaSpelly[2];}
        }
        //Elementální slevy
        //Pořadí: 1=Igni, Aqua, Terra, Aer, Nhi, 6=Phre, 0=Bez Elementu
        //Doufám, že to pole je na začátku vynulovaný?
        //Default je nula. Pokud uživatel cokoliv napíše, tak se default přepíše.
        FoundData=((EditText)Act.findViewById(R.id.editTextActionIgni)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevy[1] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevy[1]=Act.MojePostava.ElementSlevy[1];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextActionAqua)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevy[2] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevy[2]=Act.MojePostava.ElementSlevy[2];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextActionTerra)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevy[3] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevy[3]=Act.MojePostava.ElementSlevy[3];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextActionAer)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevy[4] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevy[4]=Act.MojePostava.ElementSlevy[4];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextActionNhi)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevy[5] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevy[5]=Act.MojePostava.ElementSlevy[5];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextActionPhre)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevy[6] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevy[6]=Act.MojePostava.ElementSlevy[6];}}

        FoundData=((EditText)Act.findViewById(R.id.editTextRitualIgni)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevyRitual[1] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevyRitual[1]=Act.MojePostava.ElementSlevyRitual[1];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRitualAqua)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevyRitual[2] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevyRitual[2]=Act.MojePostava.ElementSlevyRitual[2];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRitualTerra)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevyRitual[3] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevyRitual[3]=Act.MojePostava.ElementSlevyRitual[3];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRitualAer)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevyRitual[4] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevyRitual[4]=Act.MojePostava.ElementSlevyRitual[4];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRitualNhi)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevyRitual[5] = Math.abs(parseInt(FoundData));}else{if (!FirstTime){Char.ElementSlevyRitual[5]=Act.MojePostava.ElementSlevyRitual[5];}}
        FoundData=((EditText)Act.findViewById(R.id.editTextRitualPhre)).getText().toString();
        if (!FoundData.isEmpty()) {Char.ElementSlevyRitual[6] = Math.abs(parseInt(FoundData)); }else{if (!FirstTime){Char.ElementSlevyRitual[6]=Act.MojePostava.ElementSlevyRitual[6];}}
        //Pokud má slevy na rituály, Typ=2. Pokud na akční, Typ=1, Jinak 0
        //Protože všechny jsou Math.abs(), můžu použít sumu.
        int RunningTotal=0;
        int RunningRitualTotal=0;
        for (int i=0;i<7;i++){
            RunningTotal+=Char.ElementSlevy[i];
            RunningRitualTotal+=Char.ElementSlevyRitual[i];
        }
        if (RunningRitualTotal>0){
            Char.TypElementalSlev=2;
        }else{
            if(RunningTotal>0){
                Char.TypElementalSlev=1;
            }else{
                Char.TypElementalSlev=0;
            }
        }
        //Enable Regen
        if (FirstTime){Char.RegenEnabled=true;}else{Char.RegenEnabled=Act.MojePostava.RegenEnabled;}
        if (!FirstTime) {
            Char.Skilly=Act.MojePostava.Skilly; //Není kopie, jen přesměrování pointeru. Přesně, co chceme.
            Char.QuickSpellNames=Act.MojePostava.QuickSpellNames;
        }
        //And overwrite it all at once.
        Char.CDTStartTime=System.currentTimeMillis();
        Char.OverbuffStartTime=0;
        Char.GloomLevel=0;
        Act.CDTMain=null;
        Act.CDTOverbuff=null; // Tohle by mělo opravit ten bug se záporným časem!
        if (Act.MojePostava!=null){
        Char.GloomBiasCast=Act.MojePostava.GloomBiasCast;
        Char.GloomBiasPrice=Act.MojePostava.GloomBiasPrice;
        }else{
            Char.GloomBiasCast=0;
            Char.GloomBiasPrice=0;
        }
        Act.MojePostava=Char;
        Char.PamatujSi(" Já jsem nová postava! Ahoj!");
        Act.WriteToFile("Postava.txt",MainActivity.convertDocumentToString(Act.MojePostava.SaveSelfXMLDoc()));
        Act.WriteToFile("Skilly.txt",MainActivity.convertDocumentToString(Act.MojePostava.SaveSkillsXMLDoc()));
        Act.WriteLogsToFile("Log.txt",Act.MojePostava.LogUdalosti);


    }
    public void toggleVisibility(int ElementID){

        if (getActivity().findViewById(ElementID).getVisibility()==View.VISIBLE){
            getActivity().findViewById(ElementID).setVisibility(View.GONE);
        }else{
            getActivity().findViewById(ElementID).setVisibility(View.VISIBLE);
        }
    }

}