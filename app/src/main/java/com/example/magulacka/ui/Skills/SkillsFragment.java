package com.example.magulacka.ui.Skills;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.magulacka.MainActivity;
import com.example.magulacka.Postava;
import com.example.magulacka.R;
import com.example.magulacka.Skill;
import com.example.magulacka.SkillEffectTimer;
import com.example.magulacka.TimerSkill;
import com.example.magulacka.databinding.FragmentSkillsBinding;

import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SkillsFragment extends Fragment{

    MainActivity Act;

    public SkillsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (((MainActivity)getActivity()).MojePostava==null){
            Toast.makeText(getContext(), "Samí lidi, žádní Jiní. Vytvoř postavu, uživateli, a ulož ji. Pak ti dovolím přepnout z této obrazovky.", Toast.LENGTH_LONG).show();
            NavController Nav;
            Nav= NavHostFragment.findNavController(this);
            Nav.navigate(R.id.navigation_charedit);
        }
        Act=(MainActivity)getActivity();
    }
    private FragmentSkillsBinding binding;
    Skill SelectedSkill;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSkillsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        //Buttons
        root.findViewById(R.id.buttonAddSkill).setOnClickListener(view -> InputSkill(0));
        root.findViewById(R.id.buttonSkillImport).setOnClickListener(view -> importSkills());
        root.findViewById(R.id.buttonSkillExport).setOnClickListener(view -> exportSkills());
        root.findViewById(R.id.buttonDeleteSkill).setOnClickListener(view -> SmazatSkill(view));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            root.findViewById(R.id.buttonSkillSort).setOnClickListener(view -> SortSkillsClick());
            ((Button)root.findViewById(R.id.buttonSkillSort)).setText("Seřadit");
        }else{
            root.findViewById(R.id.buttonSkillSort).setEnabled(false);
            ((Button)root.findViewById(R.id.buttonSkillSort)).setText("PLACE HODOR!");
        }
        skillListRefresh();
        return root;
    }
    public void SmazatSkill(View v) {
        if (SelectedSkill == null) {
            Toast.makeText(getContext(), "Není vybrán žádný skill", Toast.LENGTH_SHORT).show();
            //DEBUG CODE
            if (Act.MojePostava.Jmeno.equals("Test")) {
                ArrayList<Skill> list = new ArrayList<>();
                Toast.makeText(getContext(), "LOADING DEBUG SKILLS", Toast.LENGTH_SHORT).show();
                list.add(new Skill("Dangexerova obrna", 0, 1, 4, 1));
                list.add(new Skill("Hustokrutorituál", 150, 2, 5, 10));
                list.add(new Skill("Funky skill", 69, 0, 3, 0));
                list.add(new TimerSkill("Buffmaster", 10, 2, 3, 1, 1, 60, 30, 0));
                list.add(new TimerSkill("Overbuffmaster", 1, 0, 3, 1, 2, 300, 1.5, 0));
                list.add(new TimerSkill("Dangexuck", 15, 1, 3, 1, 3, 20, 10, 0));
                Act.MojePostava.Skilly = list;
            }

        } else {
            for (SkillEffectTimer SET: Act.SkillEffectList) {
                if (SET.Source==SelectedSkill){
                    SET.cancel();
                    Act.SkillEffectList.remove(SET);
                    break;
                }
            }
            for (int i = 0; i < 3; i++) {
                if (Act.MojePostava.QuickSpellNames[i].equals(SelectedSkill.Jmeno)) {
                    Act.MojePostava.QuickSpellNames[i] = "";
                }
            }
            Act.MojePostava.Skilly.remove(SelectedSkill);
            SelectedSkill = null;
            skillListRefresh();
        }
    }
    //Comparatory pro řažení skillů
    public static Comparator<Skill> AZNameComparator = (S1, S2) -> S1.Jmeno.compareTo(S2.Jmeno);
    public static Comparator<Skill> ZANameComparator = (S1, S2) -> S2.Jmeno.compareTo(S1.Jmeno);
    public static Comparator<Skill> AZSkillTypeComparator = (S1, S2) -> {
        if (S1.Typ!=S2.Typ){return S1.Typ-S2.Typ;}
        if (S1.Element!=S2.Element){return S1.Element-S2.Element;}
        return S1.Jmeno.compareTo(S2.Jmeno);
    };
    public static Comparator<Skill> ZASkillTypeComparator = (S1, S2) -> {
        if (S1.Typ!=S2.Typ){return S2.Typ-S1.Typ;}
        if (S1.Element!=S2.Element){return S2.Element-S1.Element;}
        return S2.Jmeno.compareTo(S1.Jmeno);
    };
    public static Comparator<Skill> ASCCostComparator = (S1, S2) -> (S1.Cena-S2.Cena);
    public static Comparator<Skill> DESCCostComparator = (S1, S2) -> (S2.Cena-S1.Cena);
    //END Comparatory


    int Sorting=0;
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void SortSkillsClick(){
        switch(Sorting){
            case 0: //Sort A-Z
                ((Button)(binding.getRoot().findViewById(R.id.buttonSkillSort))).setText("Abeceda A-Z");
                Act.MojePostava.Skilly.sort(AZNameComparator);
                break;
            case 1://Sort Z-A
                ((Button)(binding.getRoot().findViewById(R.id.buttonSkillSort))).setText("Abeceda Z-A");
                Act.MojePostava.Skilly.sort(ZANameComparator);
                break;
            case 2://Sort A-Z Separating Skilltypes
                ((Button)(binding.getRoot().findViewById(R.id.buttonSkillSort))).setText("Druh A-Z");
                Act.MojePostava.Skilly.sort(AZSkillTypeComparator);
                break;
            case 3://Sort Z-A Separating Skilltypes
                ((Button)(binding.getRoot().findViewById(R.id.buttonSkillSort))).setText("Druh Z-A");
                Act.MojePostava.Skilly.sort(ZASkillTypeComparator);
                break;
            case 4://Sort by cost ASC
                ((Button)(binding.getRoot().findViewById(R.id.buttonSkillSort))).setText("Cena Stoupající");
                Act.MojePostava.Skilly.sort(ASCCostComparator);
                break;
            case 5://Sort by cost DESC
                ((Button)(binding.getRoot().findViewById(R.id.buttonSkillSort))).setText("Cena klesající");
                Act.MojePostava.Skilly.sort(DESCCostComparator);
                break;
        }
        Sorting=(Sorting+1)%6; //Add one
        skillListRefresh();
    }

    public void importSkills(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Import");
        builder.setMessage("Máš již vytvořený seznam skillů? Zkopíruj ho sem. Přepíšu ten současný.");
        // Set up the input
        final LinearLayout Layout = new LinearLayout(getContext());
        Layout.setOrientation(LinearLayout.VERTICAL);
        final EditText SkillInput = new EditText(getContext());
        SkillInput.setHint("<SPELLBOOK>...</SPELLBOOK>");
        SkillInput.setVerticalScrollBarEnabled(true);
        SkillInput.setMaxLines(10);
        SkillInput.setMovementMethod(new ScrollingMovementMethod());
        Layout.addView(SkillInput);
        builder.setView(Layout);
        // Set up the buttons
        builder.setPositiveButton("Importovat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!(SkillInput.getText().toString().isEmpty())) {
                    //Zahoď starý, načti nový.
                    if(Act.MojePostava.LoadSkillsXML(SkillInput.getText().toString())) { //Pokud se úspěšně načteš
                        SelectedSkill=null;
                        Act.WriteToFile("Skilly.txt",MainActivity.convertDocumentToString(Act.MojePostava.SaveSkillsXMLDoc())); //Ulož se
                    }else{
                        Toast.makeText(getContext(),"Něco se nepovedlo. Chyba? Překlep v zadávaném textu?", Toast.LENGTH_LONG).show();
                    }
                    skillListRefresh();
                }
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public void exportSkills(){
        ClipboardManager myClipBoard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        String SkillsToClip = MainActivity.convertDocumentToString(Act.MojePostava.SaveSkillsXMLDoc());
        ClipData clip = ClipData.newPlainText("", SkillsToClip );
        myClipBoard.setPrimaryClip(clip);
        Toast.makeText(getContext(),"Seznam Skillů zkopírován do clipboardu. Je možné ho uložit.", Toast.LENGTH_LONG).show();
    }

    //Ukradeno z původní Magulačky
    private Skill NewSkill;
    public void InputSkill(int PoradiDialogu){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        switch (PoradiDialogu) {
            case 0:
                NewSkill =new Skill(); //Ať je kam to ukládat
                builder.setTitle("Název");
                // Set up the input
                input.setText("");
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setText("");
                builder.setMessage("Jak se nová dovednost jmenuje?");
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewSkill.Jmeno=input.getText().toString();
                        if (NewSkill.Jmeno.isEmpty()) {
                            NewSkill.Jmeno="Bez názvu";}
                        // Jméno musí být unikátní
                        for (Skill Sk: Act.MojePostava.Skilly ) {
                            if (Sk.Jmeno.equals(NewSkill.Jmeno)) {
                                Toast.makeText(getContext(),"Jméno musí být unikátní.", Toast.LENGTH_LONG).show();
                                dialog.cancel();
                                return;
                            }
                        }
                        InputSkill(1);
                    }
                });
                builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case 1:
                String[] Druhy = {"Schopnost","Kouzlo","Rituál","Tvorba baterky"};
                ListView listview= new ListView(getContext());
                ListAdapter Adap = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_single_choice, Druhy);
                listview.setAdapter(Adap);
                listview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        v.setSelected(true);
                        NewSkill.Typ=position;
                    }
                });
                builder.setTitle("Druh");
                builder.setMessage("Je to schopnost, kouzlo akční magie či rituál? Pokud se jedná o tvorbu energetického amuletu, zvolte baterku. (Pokud není zvolen typ, předpokládá se schopnost)");
                builder.setView(listview);
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (NewSkill.Typ) {
                            case 0:
                                //Všechny skilly musí stát alespoň 1mg.
                                NewSkill.MinCena = 1;
                                break;
                            case 1:
                                NewSkill.MinCena = 1;
                                break;
                            case 2:
                                NewSkill.MinCena = 10;
                                break;
                            case 3:
                                //Baterka je třeba řešit jinak. Branch.
                                BaterkaDialog();
                                return;
                            case 4:
                                // TODO EFEKT - MinCena nebude žádná. Je možné COKOLIV, třeba lámání baterky.
                                break;
                        }
                        InputSkill(2);
                    }
                });
                builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

                break;
            case 2:
                builder.setTitle("Cena");
                // Set up the input
                input.setText("");
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_NUMBER );
                builder.setMessage("Kolik stojí v základu beze slev? (Pravidlová cena)");
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            NewSkill.Cena = Integer.parseInt(input.getText().toString());
                        } catch (Exception e) {
                            NewSkill.Cena = 0;
                        }
                        if (NewSkill.Cena < NewSkill.MinCena) {
                            NewSkill.Cena = NewSkill.MinCena;
                            Toast.makeText(getContext(), "S tímto skillem není možné jít s cenou pod "+ NewSkill.MinCena+"mg. Nastavuji na minimum.", Toast.LENGTH_LONG).show();
                        }
                        InputSkill(3);
                    }
                });
                builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case 3:
                ListView ElemView= new ListView(getContext());
                ListAdapter ElemAdap = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_single_choice, MainActivity.SkillElements);
                ElemView.setAdapter(ElemAdap);
                ElemView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
                ElemView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        v.setSelected(true);
                        NewSkill.Element=position;
                    }
                });
                builder.setTitle("Element");
                builder.setMessage("K jakému elementu patří?");
                builder.setView(ElemView);
                // Set up the buttons
                builder.setPositiveButton("Uložit!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InputSkill(4);
                    }
                });
                builder.setNeutralButton("Uložit a přidat časovač", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    TimerDialog(0);
                }
            });
                builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

                break;
            default: //Mělo by být číslo 4 nebo cokoliv dalšího
                Act.MojePostava.Skilly.add(NewSkill);
                Act.WriteToFile("Skilly.txt",MainActivity.convertDocumentToString(Act.MojePostava.SaveSkillsXMLDoc())); //Drž seznam skillů aktuální.
                skillListRefresh();
                break;
        }
    }
    public void BaterkaDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        builder.setTitle("Tvorba baterky");
        builder.setMessage("Kolik energie do baterky chci umístit?");
        // Set up the input
        input.setText("");
        // Specify the type of input expected
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText("");
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NewSkill.Cena=Integer.parseInt(input.getText().toString())*2 +30;
                NewSkill.MinCena=Integer.parseInt(input.getText().toString())+10;
                NewSkill.Typ=2;
                NewSkill.Element=2;
                InputSkill(4);
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    TimerSkill NewTimerSkill;
    public void TimerDialog(int TypTimeru){
        AlertDialog.Builder builder;
        final EditText input;
        switch (TypTimeru){
            case 0:
                NewTimerSkill = new TimerSkill(NewSkill,0,0,0,0);
                builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Druh časovače");
                builder.setMessage(
                    "Nějaký efekt ve hře ti může dočasně zvednout MaxMg."+System.lineSeparator() +
                    "Případně ti změnit násobitel přečerpání. (tj. Kolikrát více energie udržíš při přečerpání.) "+System.lineSeparator() +
                    "Poslední možnost jsou opakované platby až do vypnutí jako v případě Dangexerovy obrany."+System.lineSeparator()
                    +"Jaký časovač to bude?");
                // Set up the input
                // Set up the buttons
                builder.setPositiveButton("Změna MaxMg", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewTimerSkill.TimerEffect=1;
                        TimerDialog(1);
                    }
                });
                builder.setNeutralButton("Změna násobitele přečerpání", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewTimerSkill.TimerEffect=2;
                        TimerDialog(2);
                    }
                });
                builder.setNegativeButton("Opakované platby", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewTimerSkill.TimerEffect=3;
                        TimerDialog(3);
                    }
                });
                builder.show();
                break;
            case 1:
                builder = new AlertDialog.Builder(getContext());
                input = new EditText(getContext());
                builder.setTitle("Změna MaxMg");
                builder.setMessage("O kolik se ti zvedne kapacita?");
                // Set up the input
                input.setText("");
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setText("");
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!input.getText().toString().isEmpty()){
                            NewTimerSkill.EffectIntensity=Integer.parseInt(input.getText().toString());
                        }
                        TimerDialog(4);
                    }
                });
                builder.setNegativeButton("Zrušit timer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InputSkill(4);
                    }
                });
                builder.show();
                break;
            case 2:
                builder = new AlertDialog.Builder(getContext());
                input = new EditText(getContext());
                builder.setTitle("Změna násobitele přečerpání");
                builder.setMessage("O kolik kapacit se ti zvětší overbuff? (Příklad: Předtím udržím 2.5 násobek vlastní kapacity a poté čtyřnásobek, tak o 1.5)");
                // Set up the input
                input.setText("");
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setText("");
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!input.getText().toString().isEmpty()){
                        NewTimerSkill.EffectIntensity=Double.parseDouble(input.getText().toString());
                        } //else zůstane nula...
                        TimerDialog(4);
                    }
                });
                builder.setNegativeButton("Zrušit timer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InputSkill(4);
                    }
                });
                builder.show();
                break;
            case 3:
                builder = new AlertDialog.Builder(getContext());
                input = new EditText(getContext());
                builder.setTitle("Periodické platby");
                builder.setMessage("Kolik magů si to vezme s každým intervalem?");
                // Set up the input
                input.setText("");
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setText("");
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!input.getText().toString().isEmpty()){
                        NewTimerSkill.EffectIntensity=Integer.parseInt(input.getText().toString());
                        }
                        TimerDialog(4);
                    }
                });
                builder.setNegativeButton("Zrušit timer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InputSkill(4);
                    }
                });
                builder.show();
                break;
                // Určení času a závěr
            case 4:
                builder = new AlertDialog.Builder(getContext());
                input = new EditText(getContext());
                builder.setTitle("Čas");
                builder.setMessage("A jak dlouho to bude trvat? (V případě periodických plateb jak často probíhají?");
                // Set up the input
                input.setText("");
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setText("");
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton("Hodin", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewTimerSkill.TimerLen=3600*Long.parseLong(input.getText().toString());
                        NewSkill =NewTimerSkill; //Injekce timeru.
                        InputSkill(4);
                    }
                });
                builder.setNegativeButton("Minut", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewTimerSkill.TimerLen=60*Long.parseLong(input.getText().toString());
                        NewSkill =NewTimerSkill; //Injekce timeru.
                        InputSkill(4);
                    }
                });
                builder.setNeutralButton("Zrušit timer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InputSkill(4);
                    }
                });
                builder.show();
                break;
        }

    }

    public void skillListRefresh() {
        String[] Polozky = new String[Act.MojePostava.Skilly.size()];
        for (int i=0;i<Act.MojePostava.Skilly.size();i++) {
            Skill TheSkill=Act.MojePostava.Skilly.get(i);
            if (TheSkill.Typ == 0) {
                Polozky[i] = TheSkill.Jmeno + " [" + TheSkill.Cena + "]("+Act.ZjistiRealCenu(TheSkill)+")";
            }
            if (TheSkill.Typ == 1) {
                Polozky[i]=MainActivity.SkillElements[TheSkill.Element]+": "+TheSkill.Jmeno+" ["+TheSkill.Cena+"]("+Act.ZjistiRealCenu(TheSkill)+")";
            }
            if (TheSkill.Typ == 2) {
                Polozky[i]="R:"+MainActivity.SkillElements[TheSkill.Element]+": "+TheSkill.Jmeno+" ["+TheSkill.Cena+"]("+Act.ZjistiRealCenu(TheSkill)+")";
            }

        }
        ListView listview= ((ListView)binding.getRoot().findViewById(R.id.listViewSkillEdit));
        ListAdapter Adap = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_single_choice,Polozky);
        listview.setAdapter(Adap);
        listview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                v.setSelected(true);
                SelectedSkill=Act.MojePostava.Skilly.get(position);
            }
        });
    }
}