package com.example.magulacka.ui.cast;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.magulacka.MainActivity;
import com.example.magulacka.R;
import com.example.magulacka.Skill;
import com.example.magulacka.TimerSkill;
import com.example.magulacka.databinding.FragmentCastBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CastFragment extends Fragment {

    Timer TextUpdater;
    private FragmentCastBinding binding;
    skillGalleryAdapter adapter;
    RecyclerView recyclerView;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentCastBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (((MainActivity)getActivity()).MojePostava==null) {
            Toast.makeText(getContext(), "Samí lidi, žádní Jiní. Vytvoř postavu, uživateli, a ulož ji. Pak ti dovolím přepnout z této obrazovky.", Toast.LENGTH_LONG).show();
            NavController Nav;
            Nav = NavHostFragment.findNavController(this);
            Nav.navigate(R.id.navigation_charedit);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    @Override
    public void onPause() {
        MainActivity Act= (MainActivity)getActivity();
        if(Act.MojePostava!=null) {
            Act.WriteToFile("Postava.txt", MainActivity.convertDocumentToString(Act.MojePostava.SaveSelfXMLDoc()));
            Act.WriteToFile("Skilly.txt", MainActivity.convertDocumentToString(Act.MojePostava.SaveSkillsXMLDoc()));
        }
        //Clear all updaters!
        TextUpdater.cancel();
        TextUpdater=null;
        super.onPause();
    }

    public class UpdateTexts extends TimerTask{

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    for (skillViewHolder SVH : adapter.Updaters){
                        if (SVH.linkedSkill.getClass() == TimerSkill.class){
                            SVH.UpdateSelf();
                        }
                    }
                }
            });

        }
    }
    @Override
    public void onResume(){
        super.onResume();
        //Copied Code
        MainActivity Act= (MainActivity)getActivity();
        recyclerView = (RecyclerView)binding.getRoot().findViewById( R.id.RecyclerViewSkills);
        adapter = new skillGalleryAdapter(Act);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager( new LinearLayoutManager(getContext()));
        //End Copied code
        TextUpdater= new Timer();
        UpdateTexts NewTask = new UpdateTexts();
        TextUpdater.schedule(NewTask,1,1000);
    }
    private List<Skill> getSampleData()
    {
        List<Skill> list = new ArrayList<>();
        list.add( new Skill("Dangexerova obrna",0,1,4,1));
        list.add( new Skill("Hustokrutorituál",150,2,5,10));
        list.add( new Skill("Funky skill",69,0,3,0));
        list.add( new TimerSkill("Buffmaster",10,2,3,1,1,60,30,0));
        list.add( new TimerSkill("Overbuffmaster",1,0,3,1,2,300,1.5,0));
        list.add( new TimerSkill("Dangexuck",15,1,3,1,3,20,10,0));
        return list;
    }
}