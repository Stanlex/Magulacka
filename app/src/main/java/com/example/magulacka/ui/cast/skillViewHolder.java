package com.example.magulacka.ui.cast;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.magulacka.R;
import com.example.magulacka.Skill;
import com.example.magulacka.TimerSkill;

public class skillViewHolder extends RecyclerView.ViewHolder {
    TextView NameView; // Ex. Dangexerova obrana (100mg)
    TextView StateView; // Ex. Buff 1:54:30, +1.5kap / Vypnuto 2:0:0:0, +30MaxMg / Aktivní 12:34, (50mg) // (Nic)
    Button CastButton; // Seslat/Zapnout/Vypnout
    int index;
    View view;
    Skill linkedSkill;


    String Hodiny;
    String Minuty;
    String Sekundy;
    void SetTimeStrings(long Seconds){
        Hodiny=Long.toString(Seconds / 3600);
        Minuty=Long.toString((Seconds/60)%60);
        if (Minuty.length()==1){Minuty="0"+Minuty;}
        Sekundy=Long.toString(Seconds%60);
        if (Sekundy.length()==1){Sekundy="0"+Sekundy;}
    }
    public void UpdateSelf(){ // Tohle se volá JEN u TimerSkillů
        TimerSkill TS=(TimerSkill)linkedSkill;
        if (!TS.Active()){
            SetTimeStrings(TS.TimerLen);
            switch (TS.TimerEffect){
                case 1:
                    StateView.setText("VYP "+Hodiny+":"+Minuty+":"+Sekundy+", +"+(int)TS.EffectIntensity+"MaxMg");
                    break;
                case 2:
                    StateView.setText("VYP "+Hodiny+":"+Minuty+":"+Sekundy+", +"+TS.EffectIntensity+"kap.");
                    break;
                case 3:
                    StateView.setText("VYP "+Hodiny+":"+Minuty+":"+Sekundy+", ("+(int)TS.EffectIntensity+"mg).");
                    break;
                case 4:
                    StateView.setText("Jednou za "+Hodiny+":"+Minuty+":"+Sekundy+".");
                    break;
            }
        }else{ //TS.Active!
            SetTimeStrings(TS.ActivationTime+TS.TimerLen-System.currentTimeMillis()/1000);
            switch (TS.TimerEffect){
                case 1:
                    StateView.setText("ZAP "+Hodiny+":"+Minuty+":"+Sekundy+", +"+(int)TS.EffectIntensity+"MaxMg");
                    break;
                case 2:
                    StateView.setText("ZAP "+Hodiny+":"+Minuty+":"+Sekundy+", +"+TS.EffectIntensity+"kap.");
                    break;
                case 3:
                    StateView.setText("ZAP "+Hodiny+":"+Minuty+":"+Sekundy+", ("+(int)TS.EffectIntensity+"mg).");
                    break;
                case 4:
                    StateView.setText("Čekej "+Hodiny+":"+Minuty+":"+Sekundy+".");
                    break;
            }
        }
    }

    skillViewHolder(View itemView)
    {
        super(itemView);
        NameView = (TextView)itemView.findViewById(R.id.textViewName);
        StateView =(TextView)itemView.findViewById(R.id.textViewInfo);
        CastButton = (Button)itemView.findViewById(R.id.buttonCast);
        view = itemView;
    }

}
