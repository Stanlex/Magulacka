package com.example.magulacka.ui.cast;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.magulacka.MainActivity;
import com.example.magulacka.R;
import com.example.magulacka.Skill;

import java.util.ArrayList;
import java.util.List;

public class skillGalleryAdapter
        extends RecyclerView.Adapter<skillViewHolder> {

    List<Skill> list;
    MainActivity Activity;
    List<skillViewHolder> Updaters;

    public skillGalleryAdapter(MainActivity Activity)
    {
        this.list = Activity.MojePostava.Skilly;
        this.Activity=Activity;
        Updaters=new ArrayList<>();
    }

    @NonNull
    @Override
    public skillViewHolder
    onCreateViewHolder(ViewGroup parent,
                       int viewType)
    {
        Context context
                = parent.getContext();
        LayoutInflater inflater
                = LayoutInflater.from(context);

        // Inflate the layout
        View photoView
                = inflater
                .inflate(R.layout.skill_card,
                        parent, false);

        skillViewHolder viewHolder
                = new skillViewHolder(photoView);
        Updaters.add(viewHolder);
        return viewHolder;
    }

    // Ex. Dangexerova obrana (100)
    // Ex. ZAP 1:54:30, +1.5kap / VYP 2:0:0:0, +30MaxMg / ZAP 12:34, (50mg) // (Nic)
    // Seslat/Zapnout/Vypnout

    @Override
    public void
    onBindViewHolder(final skillViewHolder viewHolder,
                     final int position)
    {
        final int index = viewHolder.getAdapterPosition();
        viewHolder.linkedSkill=list.get(index);
        viewHolder.index=index;
        viewHolder.NameView
                .setText(viewHolder.linkedSkill.Jmeno + " ("+Activity.ZjistiRealCenu(viewHolder.linkedSkill)+")");
        viewHolder.StateView
                .setText(""); //Tohle vůbec není potřeba. Přepíše se to o vteřinu později.
        viewHolder.CastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Activity.ZkusPouzitSkill(viewHolder.linkedSkill);
                //Případný Effect timer zapne aktivita.
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }
}
