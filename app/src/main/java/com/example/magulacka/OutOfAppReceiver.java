package com.example.magulacka;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Set;

public class OutOfAppReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras=intent.getExtras();
        String Title = extras.getString("TITLE");
        String Text = extras.getString("MESSAGE");
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        Intent notificationIntent = new Intent(context, MainActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent newIntent = PendingIntent.getActivity(context, 100,
                notificationIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "channel_id")
                .setContentText(Text)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(Text))
                .setSmallIcon(R.drawable.ic_skill)
                .setContentTitle(Title)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(newIntent);
        notificationManager.notify(0, builder.build());
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();
    }
}
