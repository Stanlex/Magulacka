package com.example.magulacka;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class TimerSkill extends Skill {
    public int TimerEffect; // 1=Změna MaxMg, 2=ZměnaOverbuffMultiplieru, 3=Periodické platby, 4=Cooldown
    public long TimerLen;
    public double EffectIntensity;
    public boolean Active(){return (ActivationTime!=0);}
    public long ActivationTime;

    public TimerSkill (Skill Vzor, int TimerEffect, long TimerLen, double EffectIntensity, long ActivationTime){
        super(Vzor.Jmeno,Vzor.Cena,Vzor.Typ,Vzor.Element,Vzor.MinCena);
        this.TimerEffect=TimerEffect;
        this.EffectIntensity=EffectIntensity;
        this.TimerLen=TimerLen;
        this.ActivationTime=ActivationTime;
    }
    public TimerSkill(String Jmeno, int Cena, int Typ, int Element, int MinCena, int TimerEffect, long TimerLen, double EffectIntensity, long ActivationTime){
        super(Jmeno,Cena,Typ,Element,MinCena);
        this.TimerEffect=TimerEffect;
        this.EffectIntensity=EffectIntensity;
        this.TimerLen=TimerLen;
        this.ActivationTime=ActivationTime;
    }
    @Override
    public Element SaveSelfXML(Document SkillDoc){
        try{
        Element Result=super.SaveSelfXML(SkillDoc);
        Element timerDetails = SkillDoc.createElement("TIMER");
        timerDetails.setAttribute("Effect", String.valueOf(TimerEffect));
        timerDetails.setAttribute("Length", String.valueOf(TimerLen));
        timerDetails.setAttribute("Intensity", String.valueOf(EffectIntensity));
        timerDetails.setAttribute("ActivationTime", String.valueOf(ActivationTime));
        Result.appendChild(timerDetails);
        return Result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
