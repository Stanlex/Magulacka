package com.example.magulacka;

import android.app.Activity;
import android.os.CountDownTimer;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.magulacka.ui.cast.skillGalleryAdapter;

public class SkillEffectTimer extends CountDownTimer {
    public TimerSkill Source;
    MainActivity Magulacka;
    int skillEffect; // 1 = Zmena MaxMg, 2=Overbuff, 3=PeriodickePlatby
    double EffectIntensity;
    long TimerLen;

    public SkillEffectTimer(long millisInFuture, long countDownInterval, TimerSkill Effect, MainActivity Magulacka) {
        super(millisInFuture, countDownInterval);
        this.skillEffect=Effect.TimerEffect;
        this.EffectIntensity=Effect.EffectIntensity;
        this.TimerLen=Effect.TimerLen;
        this.Magulacka=Magulacka;
        Source=Effect;
    }

    @Override
    public void onTick(long l) {

    }

    @Override
    public void onFinish() {
        if (Source.Active()){
            switch (skillEffect){
                case 1:
                    Magulacka.MojePostava.MaxMg-=(int)EffectIntensity;
                    Source.ActivationTime=0;
                    Magulacka.NastavMagy(Magulacka.MojePostava.AktMg); //Refresh timerů.
                    break;
                case 2:
                    Magulacka.MojePostava.OverbuffMultiplier-=EffectIntensity;
                    Source.ActivationTime=0;
                    Magulacka.NastavMagy(Magulacka.MojePostava.AktMg); //Refresh timerů.
                    break;
                case 3:
                    if (Magulacka.MojePostava.AktMg<EffectIntensity){
                        Source.ActivationTime=0;
                        break;
                    }
                    Magulacka.NastavMagy(Magulacka.MojePostava.AktMg-(int)EffectIntensity);
                    Source.ActivationTime+= Source.TimerLen;
                    Magulacka.SkillEffectList.remove(this);
                    SkillEffectTimer NewTimer=new SkillEffectTimer((Source.ActivationTime+ Source.TimerLen)*1000L, 1000,Source,Magulacka);
                    NewTimer.start();
                    Magulacka.SkillEffectList.add(NewTimer);
                    break;
                default:
                    break;
            }
        }
    }
}
