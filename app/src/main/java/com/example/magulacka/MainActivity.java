package com.example.magulacka;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.util.VersionInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.magulacka.databinding.ActivityMainBinding;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class MainActivity extends AppCompatActivity {
    public enum skillTypes {
        Spell,
        Ritual,
        Ability,
        BatteryMake,
        BatteryUse
    }
    public enum timerTypes {
        AddMaxMg,
        AddOverBuffMultiplier,
        PeriodicPay,
        Cooldown
    }
    public static String[] SkillElements= {"None","Igni","Aqua","Terra","Aer","Nhi","Phre"};
    // Práce s XML
    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String convertDocumentToString(Document xmlDoc) {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(xmlDoc), new StreamResult(writer));
            String Result = writer.getBuffer().toString(); // .replaceAll("\n|\r", "");
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private ActivityMainBinding binding;
    public Postava MojePostava;

    // Timery se NEUKLADAJI. Začátek a délka jsou uloženy u samotných skillů či u postavy v případě šera.
    // Při každém načtení je potřeba tenhle list znovu naplnit.
    public CountDownTimer CDTMain;
    public CountDownTimer CDTOverbuff;
    //V těhlech timerech se ukládají hlavně ty magy ovlivňující věci. onTick bude prázdný
    public List<SkillEffectTimer> SkillEffectList;
    public boolean NotifyEnabled=false;

    @Override
    protected void onResume() {
        //Pokud je postava NULL, nech to spadnout do Home-onResume a následně do vytváření postavy.
        //Pokud není NULL, můžou být v paměti neplatná data. Reload ze souboru: Souboru věříme.
        if (MojePostava!=null){
            //Reload ze souboru. Zahoď to, co je v paměti.
            String NactenyString = readFromFile(
                    "Postava.txt");
            if (NactenyString != null) {
                MojePostava = new Postava();
                MojePostava.LoadSelfXML(NactenyString);
                NactenyString = readFromFile("Skilly.txt");
                MojePostava.LoadSkillsXML(NactenyString);
                loadLogsFromFile("Log.txt", MojePostava);
            }
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            NotifyEnabled=sp.getBoolean("NotifyEnabled",false);

            int previousMg=MojePostava.AktMg;
            ZrychlenyTokCasu();
            MojePostava.PamatujSi("Probuzení! Z původních "+previousMg+" mg je teď "+MojePostava.AktMg+" mg.");
            startShortTimers(); //Includes SkillEffects
            if (NotifyEnabled){cancelAllNotifications();}
        }
        super.onResume();
    }
    @Override
    protected void onPause() {
        if (MojePostava!=null){
            WriteToFile("Postava.txt", convertDocumentToString(MojePostava.SaveSelfXMLDoc()));
            WriteToFile("Skilly.txt", convertDocumentToString(MojePostava.SaveSkillsXMLDoc()));
            MojePostava.PamatujSi("Jdu spát a mám "+MojePostava.AktMg+" mg.");
            WriteLogsToFile("Log.txt",MojePostava.LogUdalosti);
        }
        if (NotifyEnabled && MojePostava!=null){
            setNotifications();
        }
        for (SkillEffectTimer SET: SkillEffectList){
            SET.cancel();
        }
        SkillEffectList.clear();
        super.onPause();
    }

    public void WriteLogsToFile(String FileName, LinkedList<String> Log){
        WriteToFile(FileName,"");
        for (String LogItem: Log) {
            AppendToFile(FileName,LogItem+System.getProperty("line.separator"));
        }
    }

    AlarmManager alarmManager;
    Intent intent;
    PendingIntent pendingIntent;
    final int NotificationNumber=11;
    public void cancelAllNotifications(){
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //filterEquals does NOT care about Extradata!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (alarmManager.canScheduleExactAlarms() == false) {
                return;
            }
        }
        intent = new Intent(this, OutOfAppReceiver.class);
        for (int i=0;i<NotificationNumber;i++){
            pendingIntent = PendingIntent.getBroadcast(this, i, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
            alarmManager.cancel(pendingIntent);
            //Nothing here. Caneclling inexisting intents seems to do nothing. So I don't need to worry.
        }
    }

    public void setNotifications(){
        Boolean BeziPlatby=false;
        for (SkillEffectTimer SET : SkillEffectList){
            if (SET.skillEffect==3){ //Platby
                BeziPlatby=true;
                break;
            }
        }
        //Speciální případy
        if (MojePostava.RegenEnabled==false && MojePostava.GloomLevel==0 && !BeziPlatby){
            //Nic! Magy se nehnou!
            return;
        }
        if (MojePostava.GloomLevel==0 && MojePostava.RegenEnabled && BeziPlatby){
            //Tohle ODMÍTÁM počítat.
            //Není možné říct předem, jestli se to zacyklí. A i kdyby, tak jestli to hráče dobije nebo vysaje. Občas i obě možnosti.
            setSkillPaymentNotifications();
            return;
        }
        //Periodické tikání
        if (MojePostava.GloomLevel>0){
            setGloomNotifications();
        }
        if (BeziPlatby){
            setSkillPaymentNotifications();
        }
        if (MojePostava.AktMg>MojePostava.MaxMg && !BeziPlatby){
            setOverbuffNotifications();
        }
        //V klidu regeneruju
        if (MojePostava.AktMg<MojePostava.MaxMg && MojePostava.RegenEnabled && MojePostava.GloomLevel==0){
            long DobitiMillis = predpovedZlomu(false,MojePostava.MaxMg)*1000L-System.currentTimeMillis();
            setNotifyOnce(0, DobitiMillis, "Dobito", "Jsi na příjemné hodnotě energie. Je krásný den a nic se nemůže pokazit. (Ha. Ha. Ha.)");
            if (MojePostava.MaxMg<MojePostava.AktMg){
                //Dobití se nekoná, vypršela optimalizace.
                setOverbuffNotifications();
            }
            return;
        }
        //Pomalu se vyčerpávám
        if (BeziPlatby || MojePostava.GloomLevel>0){
            long CrashMillis = predpovedZlomu(true,MojePostava.MaxMg)*1000L-System.currentTimeMillis();
            if (MojePostava.GloomLevel>0){
                if (CrashMillis>1800*1000L) {setNotifyOnce(1, CrashMillis-1800*1000L, "Šero", "Je ti zima...");}
                setNotifyOnce(2, CrashMillis, "Vysání!", "Uvíznutí! Je ti zima... Už navždy?");
            }else{
                if (CrashMillis>1800*1000L) {setNotifyOnce(3, CrashMillis-1800*1000L, "Dochází energie!", "Udržovat aktivní schopnosti začíná být náročné.");}
                setNotifyOnce(4, CrashMillis, "Vysání!", "Došla ti síla. Ztrácíš vědomí a pouštíš soustředění na aktivní schopnosti.");
            }
        }
    }

    public void setSkillPaymentNotifications(){
        for (SkillEffectTimer SET : SkillEffectList){
            if (SET.skillEffect==3){ //Platby
                setNotifyOnce(5,(SET.Source.ActivationTime+SET.TimerLen)*1000L-System.currentTimeMillis(), SET.Source.Jmeno, "Udržování efektu tě vyčerpává.");
                setNotifyOnce(6,(SET.Source.ActivationTime+2*SET.TimerLen)*1000L-System.currentTimeMillis(), SET.Source.Jmeno, "Udržování efektu tě vyčerpává.");
           }
        }
    }
    public void setGloomNotifications(){
        long nextGloomTick= MojePostava.CDTStartTime+MojePostava.DobaGloom[MojePostava.GloomLevel]*1000L-System.currentTimeMillis();
        setNotifyOnce(7, nextGloomTick,"Šero", "Šero tě pomalu vysává...");
        setNotifyOnce(8, nextGloomTick+MojePostava.DobaGloom[MojePostava.GloomLevel]*1000L,"Šero", "Šero tě pomalu vysává...");
   }
    public void setOverbuffNotifications(){
        long overbuffExpiresInMillis = MojePostava.OverbuffStartTime+MojePostava.OverbuffDoba*1000L-System.currentTimeMillis();
        setNotifyOnce(9, overbuffExpiresInMillis-3600*1000L, "Přepětí", "Držíš v sobě velké množství energie silou vůle a dlouho už to nezvládneš...");
        setNotifyOnce(10, overbuffExpiresInMillis, "Přepětí", "Tvá vůle povolila. Přebytečná energie je pryč.");
    }

    public long predpovedZlomu(boolean goingDown, int upperBreak){
        String Loaded = readFromFile("Postava.txt");
        Postava BACKUP = new Postava();
        BACKUP.LoadSelfString(Loaded); // Právě mám kopii postavy, kdybych ji potřeboval vrátit do původního stavu.
        Loaded = readFromFile("Skilly.txt");
        BACKUP.LoadSkillsString(Loaded); // Včetně skillů (Aktivní efekty se ukládají v nich!)
        long result;
        // Cosi jako zrychlený tok času
        Queue<MagEvent> EventPriorityQueue = pripravFrontuEventu();
        boolean QueueFailed=false;
        MagEvent FirstEvent = null;
        if (goingDown){
            while (!QueueFailed){
                FirstEvent=EventPriorityQueue.poll();
                if (!FirstEvent.Fire(EventPriorityQueue)){QueueFailed=true;} //Fire the event. If it does not complete flawlessly, make a note
                if (EventPriorityQueue.peek()==null){break;}
            }
            result = FirstEvent.TimeOfEvent();
        }else { //Going UP
            while (MojePostava.AktMg < upperBreak) {
                FirstEvent = EventPriorityQueue.poll();
                if (!FirstEvent.Fire(EventPriorityQueue)) {
                    // QueueFailed = true;
                } //This should NOT happen here.
                if (EventPriorityQueue.peek() == null) {
                    break;
                }
            }
            result = FirstEvent.TimeOfEvent();
        }
        MojePostava=BACKUP; //Vrátit do původního stavu!!!!
        return result;
    }
    public void setNotifyOnce(int notifyType, long firstTimerInMillis, String title, String message){
        long nextStartTime = System.currentTimeMillis() + firstTimerInMillis;
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        intent = new Intent(this, OutOfAppReceiver.class);
        Bundle extras= new Bundle();
        extras.putString("TITLE", title);
        extras.putString("MESSAGE", message);
        intent.putExtras(extras);
        pendingIntent = PendingIntent.getBroadcast(this, notifyType, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, nextStartTime,pendingIntent);
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, nextStartTime,pendingIntent);
        }
    }

    public int QSButtonID(int i){
        switch(i){
            case 1:
                return R.id.QuickSpell1Button;
            case 2:
                return R.id.QuickSpell2Button;
            case 3:
                return R.id.QuickSpell3Button;
            case 4:
                return R.id.QuickSpell4Button;
            case 5:
                return R.id.QuickSpell5Button;
            case 6:
                return R.id.QuickSpell6Button;
            default:
                return 0;
        }
    }
    @Override
    protected void onStart(){
        //Tohle je vstup do programu. Pokud TADY načte postavu, není to první spuštění. K ničemu jinému to neslouží a data v ní mohou být ŠPATNĚ
        if (MojePostava==null) {
            String NactenaPostava = readFromFile("Postava.txt");
            if (NactenaPostava != null) {
                MojePostava = new Postava();
                MojePostava.LoadSelfString(NactenaPostava);
                NactenaPostava = readFromFile("Skilly.txt");
                MojePostava.LoadSkillsString(NactenaPostava);
                loadLogsFromFile("Log.txt",MojePostava);
            }else{ //Pokud nenačte žádnou postavu, donuť uživatele si ji založit.

            }
        }
        super.onStart();
    }

    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
    public static String TimeStampNow() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public void loadLogsFromFile(String JmenoSouboru, Postava MojePostava)
    {
        try( BufferedReader br = new BufferedReader( new InputStreamReader(openFileInput(JmenoSouboru))))
        {
            LinkedList<String> LogReconstruction= new LinkedList<>();
            String line;
            while(( line = br.readLine()) != null ) {
                LogReconstruction.add(line);
            }
            MojePostava.LogUdalosti.clear();
            MojePostava.LogUdalosti=LogReconstruction;
        }catch(IOException e){
            Toast.makeText(getApplicationContext(), "Error reading the logs!!!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean NightEnabled=sp.getBoolean("DarkMode",false);
        if (NightEnabled)
            if(AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_YES) { //If DarkMode is enabled... No entry=false;
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        super.onCreate(savedInstanceState);
        SkillEffectList=new ArrayList<>();

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_charedit, R.id.navigation_cast,R.id.navigation_skill)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.magulacka_menu, menu);
        return true;
    }
    public void CharView(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Postava");

        builder.setMessage(MojePostava.VypisSe());
        if (MojePostava.Kategorie==42)
        {builder.setMessage(R.string.EasterEgg);
        }
        builder.show();
    }
    public void LogView(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String Title="Historie, logy. v"+ BuildConfig.VERSION_NAME;
        builder.setTitle(Title);

        StringBuilder sb = new StringBuilder();
        for (String LogItem: MojePostava.LogUdalosti) {
            sb.append(LogItem).append(System.getProperty("line.separator"));
        }
        builder.setMessage(sb);
        builder.setPositiveButton("Kopírovat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClipboardManager myClipBoard = (ClipboardManager) getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", sb );
                myClipBoard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(),"Log uložen do Clipboardu. ", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Zavřít", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public void EasterEggTry(){
        int EasterEgg = (int)(Math.random()*8); // top not included.
        switch (EasterEgg){
            case 0:
                Toast.makeText(getApplicationContext(), "A když vytuhne na Poláka, tak to je Morte-off?", Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(getApplicationContext(), "Seženu si v šeru díru, bungee lano, pár upírů...", Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(getApplicationContext(), "Ostatně soudím, že mágové by měli být nerfnuti.", Toast.LENGTH_LONG).show();
                break;
            case 3:
                Toast.makeText(getApplicationContext(), "Šly vědmičky na kafe, na kafe, na kafe...", Toast.LENGTH_LONG).show();
                break;
            case 4:
                Toast.makeText(getApplicationContext(), "Vnímám vnímatelné. Vnímám výhružky a ruku přibodnutou ke klávesnici.", Toast.LENGTH_LONG).show();
                break;
            case 5:
                Toast.makeText(getApplicationContext(), "Nejsme na Studničkově, že ne?", Toast.LENGTH_LONG).show();
                break;
            case 6:
                Toast.makeText(getApplicationContext(), "Ne, žádný jablečný kompot! Nikdy mě nedonutíte! To je horší než ananas!", Toast.LENGTH_LONG).show();
                break;
            case 7:
                Toast.makeText(getApplicationContext(), "A já tvoje nebudu, nebudu, nebudu, radši se vdám za Budu...", Toast.LENGTH_LONG).show();
                break;
        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        if (MojePostava==null) {
            Toast.makeText(getApplicationContext(),"Bez postavy tady nic fungovat nebude...", Toast.LENGTH_LONG).show();
            return super.onOptionsItemSelected(item);
        }
        int id = item.getItemId();
        if (id == R.id.ViewChar) {
            CharView();
        }
        if (id == R.id.ViewLogs) {
            LogView();
        }
        if (id == R.id.ToggleRegen) {
            if (MojePostava.RegenEnabled){
                stopRegenTimer();
                MojePostava.RegenEnabled=false;
            }else{
                MojePostava.RegenEnabled=true;
                if (MojePostava.GloomLevel==0 && MojePostava.AktMg<MojePostava.MaxMg){
                    startRegenTimer(false);
                }
            }
        }
        if (id == R.id.ToggleNotifications) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sp.edit();
            if (NotifyEnabled){
                editor.putBoolean("NotifyEnabled",false);
                editor.apply();
                NotifyEnabled=false;
                Toast.makeText(getApplicationContext(), "Notifikace vypnuty!", Toast.LENGTH_LONG).show();
            }else {
                editor.putBoolean("NotifyEnabled",true);
                editor.apply();
                NotifyEnabled=true;
                Toast.makeText(getApplicationContext(), "Notifikace zapnuty!", Toast.LENGTH_LONG).show();
            }
        }
        if (id == R.id.QuickSpellChoice) {
            QuickSpellDialog();
        }
        if (id == R.id.GloomBias)
        {
            SetGloomBiasDialog();
        }
        if (id == R.id.SwitchTheme)
        {
            changeTheme();
        }
        return super.onOptionsItemSelected(item);
    }
    public boolean ZkusPouzitSkill(Skill ToCast){
        if (ToCast.getClass()==TimerSkill.class && ((TimerSkill)ToCast).Active()){
            //Ve skutečnosti ho chci VYPNOUT.
            TimerSkill TS=(TimerSkill)ToCast;
            //Najdi Effect timer
            SkillEffectTimer FoundEffectTimer=null;
            for (SkillEffectTimer SET: SkillEffectList) {
                if (SET.Source.Jmeno.equals(TS.Jmeno)){
                    FoundEffectTimer=SET;
                    break;
                }
            }
            if (FoundEffectTimer==null){return false;} //Něco je HODNE spatne
            switch (TS.TimerEffect){
                case 3:
                    FoundEffectTimer.cancel();
                    //Další interval už neplatím. Zrušit!
                    TS.ActivationTime=0;
                    break;
                case 4:
                    Toast.makeText(getApplicationContext(), "Cooldown ještě neproběhl! Ale když jinak nedáš...", Toast.LENGTH_LONG).show();
                default:
                    FoundEffectTimer.onFinish();
                    FoundEffectTimer.cancel();
                    TS.ActivationTime=0;
                    break;
            }
            Toast.makeText(getApplicationContext(), "Vypnuto!", Toast.LENGTH_LONG).show();
            MojePostava.PamatujSi("Vypínám skill "+TS.Jmeno);
            return true;
        }
        //Elementální slevy
        int Cena=ZjistiRealCenu(ToCast);
        if (Cena>MojePostava.AktMg){
            Toast.makeText(getApplicationContext(), "Málo Energie!", Toast.LENGTH_LONG).show();
            return false;
        }else{
            NastavMagy(MojePostava.AktMg-Cena);
            String NewLog="Sesílám "+ToCast.Jmeno+" za "+Cena;
            if (Zridlo && ToCast.Typ==2) {
                NastavMagy(MojePostava.AktMg+Cena/4);
                Toast.makeText(getApplicationContext(), "Ze zřídla se ti vrací energie... "+Cena/4+" mg.", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Jo a... Kdes dneska sehnal zřídlo? Ptám se čistě z akademických důvodů...", Toast.LENGTH_LONG).show();
                NewLog=NewLog.concat(" a něco se vrátilo ze zřídla.");
            }
            if (Math.random()<0.05){    EasterEggTry();}else{Toast.makeText(getApplicationContext(), "Sesláno!", Toast.LENGTH_SHORT).show();}
            MojePostava.PamatujSi(NewLog);
        }
        //A teď ta sranda s timery
        //přidám JEN Effect Timer.
        //Obrazovka CastFragment se postará o svoje updatery sama.
        if (ToCast.getClass()==TimerSkill.class) {
            TimerSkill TS=(TimerSkill)ToCast;
            //Zapni buffy
            switch (TS.TimerEffect){
                case 1:
                    MojePostava.MaxMg+=(int)TS.EffectIntensity;
                break;
                case 2:
                    MojePostava.OverbuffMultiplier+=TS.EffectIntensity;
                    break;
                //case 3,4: netřeba. Tam žádný buffy neprobíhají
                default:
                    break;
            }
            //zapni případné ostatní timery jako je regen a overbuff
            NastavMagy(MojePostava.AktMg); //Refresh timerů.
            //Zapni timer do jejich vypršení
            TS.ActivationTime=System.currentTimeMillis()/1000;
            SkillEffectTimer NewTimer=new SkillEffectTimer(TS.TimerLen*1000L,1000,TS,this);
            NewTimer.start();
            SkillEffectList.add(NewTimer);
        }
        return true;
    }
    public void WriteToFile(String JmenoSouboru , String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(JmenoSouboru, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    public void AppendToFile(String JmenoSouboru , String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(JmenoSouboru, Context.MODE_APPEND));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    public String readFromFile(String JmenoSouboru) {
        String res = "";
        try {
            // OPENING THE REQUIRED TEXT FILE
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    openFileInput(JmenoSouboru)));

            String myLine = reader.readLine();

            // NOW READING THEM LINE BY LINE UPTO THE END OF FILE
            while (myLine != null) {
                res += myLine; // Smazal jsem případné "+/n". Chci to mit na jednom radku.
                myLine = reader.readLine();
            }

            // CLOSE THE FILE AFTER WE HAVE FINISHED READING
            reader.close();
        } catch (IOException e) {
            // INFORM USER OF ANY ERROR...
            Toast.makeText(getApplicationContext(),"Soubor k načtení nenalezen.", Toast.LENGTH_LONG).show();
            return null;
        }
        // AND FINALLY SHOW THE READ TEXT IN OUR TEXT VIEW
        return res;
    }
    public void changeTheme(){
        //Navigation.findNavController(this,R.id.nav_host_fragment_activity_main).navigate(R.id.navigation_home);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sp.edit();
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); //Dark mode was enabled. We switch it off...
            editor.putBoolean("DarkMode",false);
            editor.apply();
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            editor.putBoolean("DarkMode",true);
            editor.apply();
        }
    }

    public void stopSeroTimer(){
        if (MojePostava.GloomLevel>0 && CDTMain!=null) {
            CDTMain.cancel();
            CDTMain = null;
        }
    }
    public void stopRegenTimer(){
        if (MojePostava.GloomLevel==0 && CDTMain!=null) {
            CDTMain.cancel();
            CDTMain = null;
        }
    }
    public void stopOverbuffTimer(){
        if (CDTOverbuff!=null)
        {
            MojePostava.PamatujSi("Končí mi přebití.");
            CDTOverbuff.cancel();
            CDTOverbuff = null;
        }
    }

    public void startRegenTimer(boolean isShort){
        if (MojePostava.DobaGloom[0]==0 || !MojePostava.RegenEnabled){return;}
        if (CDTMain!=null)
        {
            CDTMain.cancel();
            CDTMain = null;
        }
        long Expiry;
        long currentTime = System.currentTimeMillis()/1000;
        if (isShort){
            Expiry=MojePostava.DobaGloom[0]-(currentTime-MojePostava.CDTStartTime/1000);
        }else{
            Expiry=MojePostava.DobaGloom[0];
        }
        CDTMain=new CountDownTimer(Expiry* 1000L,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                NastavMagy(Math.min(MojePostava.AktMg+MojePostava.RegenMg(),MojePostava.MaxMg));
                if (MojePostava.AktMg==MojePostava.MaxMg){
                    stopRegenTimer();
                }else{
                    startRegenTimer(false);
                }
            }
        }.start();
        if (!isShort){
            MojePostava.CDTStartTime =System.currentTimeMillis();
        }
    }
    public void startOverbuffTimer(boolean isShort){
        if (CDTOverbuff!=null)
        {
            CDTOverbuff.cancel();
            CDTOverbuff = null;
        }
        long Expiry;
        long currentTime = System.currentTimeMillis()/1000;
        if (isShort){
            Expiry=MojePostava.OverbuffDoba-(currentTime-MojePostava.OverbuffStartTime);
        }else{
            Expiry=MojePostava.OverbuffDoba;
        }
        CDTOverbuff=new CountDownTimer(Expiry*1000L,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                NastavMagy(MojePostava.MaxMg);
                Toast.makeText(getApplicationContext(),"Nadbytečná energie se rozutekla.", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),"Obrovská úleva...", Toast.LENGTH_LONG).show();
            }
        }.start();
        if (!isShort){
            //Short volání je při načítání. Long volání je při užívání.
            //Při short volání není třeba logovat. Overbuff už byl.
            MojePostava.OverbuffStartTime =System.currentTimeMillis();
            MojePostava.PamatujSi("Začíná mi přebití.");
            Toast.makeText(getApplicationContext(),"Přebití! Klepeš se a máš pocit, že ti vstávají všechny chlupy po tělě a energie ti jiskří mezi prsty.", Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(),"A to i těmi u nohou.", Toast.LENGTH_LONG).show();
        }

    }
    //Dá se volat i když už nějaký Šerotimer běží.
    public void startSeroTimer(boolean isShort){
        if (CDTMain!=null)
        {
            CDTMain.cancel();
            CDTMain = null;
        }
        long Expiry;
        if (isShort){
            Expiry=MojePostava.DobaGloom[MojePostava.GloomLevel]-(System.currentTimeMillis()-MojePostava.CDTStartTime)/1000;
        }else{
            Expiry=MojePostava.DobaGloom[MojePostava.GloomLevel];
        }
        CDTMain=new CountDownTimer(Expiry*1000L,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                switch (MojePostava.GloomLevel){
                    case 1:
                        NastavMagy(Math.max(MojePostava.AktMg-MojePostava.CenaGloom1Final(),0));
                        startSeroTimer(false);
                        break;
                    case 2:
                        NastavMagy(Math.max(MojePostava.AktMg-MojePostava.CenaGloom2Final(),0));
                        startSeroTimer(false);
                        break;
                }
            }
        }.start();
        if (!isShort){
            MojePostava.CDTStartTime =System.currentTimeMillis();
        }
    }

    public boolean Ohnisko=false;
    public boolean RitualSolo=false;
    public boolean Roucho=false;
    public boolean Zridlo=false;
    int RouchoSleva=20;
    public int ZjistiRealCenu(Skill TheSkill){
        int Cena=TheSkill.Cena;
        //Generické slevy na spelly
        if (TheSkill.Typ==1){
            if (Ohnisko){
                Cena-=Cena/4;
            }
            Cena -= MojePostava.SlevaSpelly[0];
            if (MojePostava.GloomLevel==1){
                Cena-=MojePostava.SlevaSpelly[1];
            }
            if (MojePostava.GloomLevel==2){
                Cena-=MojePostava.SlevaSpelly[2];
            }
            Cena-=MojePostava.GloomBiasCast;
        }
        //Vědmácké rituální slevy??
        if (TheSkill.Typ == 2) {
            if (RitualSolo) {
                Cena-=Cena/4;
            }
            if (Roucho) {
                Cena-=RouchoSleva;
            }
        }
        if(MojePostava.TypElementalSlev>0 && TheSkill.Typ==1){ //I REALLY hope this works. it should.
            Cena-=MojePostava.ElementSlevy[TheSkill.Element];
        }
        if(MojePostava.TypElementalSlev==2 && TheSkill.Typ==2){ //It did not. Fixed a few years later.
            Cena-=MojePostava.ElementSlevyRitual[TheSkill.Element];
        }
        //Schopnosti
        if (TheSkill.Typ == 0) {
            Cena-=MojePostava.GloomBiasCast;
        }
        //A konec.
        Cena=Math.max(Cena,TheSkill.MinCena);
        return Cena;
    }
    public void NastavMagy(int Pocet){
        if (Pocet<0) {Toast.makeText(getApplicationContext(), "Tolik energie nemáš! Ruším příkaz.", Toast.LENGTH_LONG).show(); return;}
        if (Pocet>MojePostava.MaxOverbuff()) {
            Toast.makeText(getApplicationContext(), "Moc energie! Část jí vyšuměla pryč.", Toast.LENGTH_LONG).show();
            Pocet=MojePostava.MaxOverbuff();
        }
        if (Pocet>=MojePostava.MaxMg){ //Jsem v Overbuffu (Byl jsem v něm už předtím?)
            MojePostava.AktMg=Pocet;

            if (MojePostava.GloomLevel==0 && CDTMain !=null)
            {
                stopRegenTimer();
            }
            if (Pocet==MojePostava.MaxMg) {
                stopOverbuffTimer();

            }
            if (Pocet>MojePostava.MaxMg && CDTOverbuff==null){ //Doteď jsem nebyl
                MojePostava.OverbuffStartTime=System.currentTimeMillis();
                Toast.makeText(getApplicationContext(), "Overbuff!", Toast.LENGTH_LONG).show();
                MojePostava.PamatujSi("Začíná mi přebití! Mám "+MojePostava.AktMg+" mg.");
                startOverbuffTimer(false);
            }
            //Pokud jsem byl, s timerem se NIC nedeje.
        }else{//Postava není plně nabitá
            MojePostava.AktMg=Pocet;
            stopOverbuffTimer();
            if (MojePostava.GloomLevel==0){//Jsem v realitě. Měl bych regenerovat.
                if (CDTMain == null){ //A doteď jsem neregenervoal
                    MojePostava.PamatujSi("Začínám regenerovat! Mám jen "+Pocet+" mg.");
                    startRegenTimer(false);}
            }
        }
    }
    public void startShortTimers(){
        //Volat po Zrychleném toku. Natartuje timery na použití.
        //Přebití
        if (MojePostava.AktMg>MojePostava.MaxMg){
            startOverbuffTimer(true);
        }
        //Šero
        if (MojePostava.GloomLevel>0){
            startSeroTimer(true);
        }else{
            //Regen
            if (MojePostava.AktMg<MojePostava.MaxMg && MojePostava.RegenEnabled){
                startRegenTimer(true);
            }
        }
        for (SkillEffectTimer SET: SkillEffectList) {
            SET.cancel();
        }
        SkillEffectList.clear();
        //Pokud jsou stále aktivní některé skilly, bylo by dobré načíst jejich timery.
        for (Skill S: MojePostava.Skilly) {
            if (S.getClass()==TimerSkill.class && ((TimerSkill) S).Active()){
                //PRACUJ!
                TimerSkill TS=(TimerSkill)S;
                SkillEffectTimer SET = new SkillEffectTimer((TS.ActivationTime+TS.TimerLen)*1000L-System.currentTimeMillis(),1000,TS,this);
                SET.start();
                SkillEffectList.add(SET);
            }

        }
    }
    //Bude se využívat pří zrychleném toku času
    public boolean NastavMagyFastTime(int Pocet, long eventTime) {
        if (Pocet<0) {MojePostava.AktMg=0; return false;}
        if (Pocet>MojePostava.MaxOverbuff()) {Pocet=MojePostava.MaxOverbuff();}
        if (Pocet>MojePostava.MaxMg){MojePostava.AktMg=Pocet;}
        if (Pocet>MojePostava.MaxMg && MojePostava.OverbuffStartTime==0){
            MojePostava.OverbuffStartTime=eventTime;
        }else{//Postava není plně nabitá
        MojePostava.AktMg=Pocet;
        MojePostava.OverbuffStartTime=0;
        }
        return true;
    }
    public static Comparator<MagEvent> TimeComparator = new Comparator<MagEvent>(){
        @Override
        public int compare(MagEvent c1, MagEvent c2) {
            return (int) (c1.TimeOfEvent() - c2.TimeOfEvent());
        }
    };
    interface MagEvent {
        long TimeOfEvent();
        boolean Fire(Queue<MagEvent> magQueue);
    }

    public Queue<MagEvent> pripravFrontuEventu(){
        Queue<MagEvent> EventPriorityQueue;
        EventPriorityQueue = new PriorityQueue<>(20, TimeComparator);
        //Jestli na někom poběží víc než 20 timerů, tak má větší problém, než Magulačka zvládne.
        for (Skill S: MojePostava.Skilly) {
            if (S.getClass()==TimerSkill.class){
                if (((TimerSkill)S).Active()){
                    TimerSkill TS = (TimerSkill)S;
                    switch (TS.TimerEffect){
                        // 1=Změna MaxMg, 2=ZměnaOverbuffMultiplieru, 3=Periodické platby.
                        case 1:
                            EventPriorityQueue.add( new MaxMgBuffExpire(TS));
                            break;
                        case 2:
                            EventPriorityQueue.add( new OverBuffBuffExpire(TS));
                            break;
                        case 3:
                            EventPriorityQueue.add( new PeriodicCost(TS));
                            break;

                    }
                }
            }

        }
        if (MojePostava.AktMg> MojePostava.MaxMg){
            //Overbuff.
            EventPriorityQueue.add( new OverBuffExpire(MojePostava.OverbuffStartTime/1000+MojePostava.OverbuffDoba));
        }

        if (MojePostava.GloomLevel>0){
            //Jsem v šeru...
            EventPriorityQueue.add( new GloomTick(MojePostava.CDTStartTime/1000+MojePostava.DobaGloom[MojePostava.GloomLevel]) );
        }

        if (MojePostava.GloomLevel==0 && MojePostava.AktMg<MojePostava.MaxMg){
            if (MojePostava.RegenEnabled){
                //Regeneruji
                EventPriorityQueue.add( new RegenTick(MojePostava.CDTStartTime/1000+MojePostava.DobaGloom[0]) );
            }
        }
        return EventPriorityQueue;
    }

    public void ZrychlenyTokCasu() {
        Queue<MagEvent> EventPriorityQueue = pripravFrontuEventu();
        boolean QueueFailed=false;
        //Najít současný čas...
        long currentTime = Calendar.getInstance().getTime().getTime()/1000;
        if (EventPriorityQueue.peek()!=null){
            long NextEventTime=EventPriorityQueue.peek().TimeOfEvent();
            MagEvent FirstEvent;
            while (!QueueFailed && NextEventTime<currentTime){
                FirstEvent=EventPriorityQueue.poll();
                if (!FirstEvent.Fire(EventPriorityQueue)){QueueFailed=true;} //Fire the event. If it does not complete flawlessly, make a note
                if (EventPriorityQueue.peek()==null){break;}
                NextEventTime=EventPriorityQueue.peek().TimeOfEvent();
            }
        }
        if(QueueFailed){
            MojePostava.AktMg=0;
            if (MojePostava.GloomLevel>0){
            Toast.makeText(getApplicationContext(),"Šero tě vysálo na kost a postava uvízla. Sbohem.",Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(),"I když spíš uživatel zapomněl Magulačce říct, že z něj dávno vylezl...",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(),"Skilly s udržovací cenou tě vysály a spadly. Postava se vydala z posledního!",Toast.LENGTH_LONG).show();
            }
        //Fronta doběhla. Teď zapnout normální timery...
        }
    }
    public class MaxMgBuffExpire implements MagEvent {
        TimerSkill TS;
        long EventTime;
        int MaxMgBuff;
        public MaxMgBuffExpire(TimerSkill TS){
            this.TS=TS;
            this.EventTime=TS.ActivationTime+TS.TimerLen;
            this.MaxMgBuff=(int)TS.EffectIntensity;
        }

        @Override
        public long TimeOfEvent() {
            return EventTime;
        }

        @Override
        public boolean Fire(Queue<MagEvent>magQueue) {
            //Pokud jsem doteď nebyl v overbuffu, právě jsem se do něj možná dostal.
            if (MojePostava.MaxMg>=MojePostava.AktMg && MojePostava.MaxMg-MaxMgBuff< MojePostava.AktMg){
                //Prave jsem se dostal do Overbuffu
                magQueue.add( new OverBuffExpire(EventTime+ MojePostava.OverbuffDoba));
                MojePostava.OverbuffStartTime=EventTime*1000L;
            }
            if (MojePostava.MaxMg-MaxMgBuff<= MojePostava.AktMg){
                //Právě jsem přestal regenerovat, páč najednou nemám kam.
                stopRegenTimer();
            }
            MojePostava.MaxMg-=MaxMgBuff;
            NastavMagyFastTime(MojePostava.AktMg, EventTime);
            TS.ActivationTime=0; //Expiroval
            return true;
        }
    }
    public class OverBuffBuffExpire implements MagEvent {
        TimerSkill TS;
        long eventTime;
        double intensity;

        public OverBuffBuffExpire(TimerSkill TS){
            this.TS=TS;
            this.eventTime=TS.ActivationTime+TS.TimerLen;
            this.intensity=TS.EffectIntensity;
        }
        @Override
        public long TimeOfEvent() {
            return eventTime;
        }

        @Override
        public boolean Fire(Queue<MagEvent> magQueue) {
            MojePostava.OverbuffMultiplier-=intensity;
            NastavMagyFastTime(MojePostava.AktMg, eventTime);
            TS.ActivationTime=0;
            return true;
        }
    }
    public class PeriodicCost implements MagEvent {
        TimerSkill TS;
        long EventTime;
        int Period;
        int Cena;

        public PeriodicCost(TimerSkill TS){
            this.TS=TS;
            this.Cena=(int)TS.EffectIntensity;
            this.Period=(int)TS.TimerLen;
            this.EventTime=TS.ActivationTime+TS.TimerLen;
        }

        @Override
        public long TimeOfEvent() {
            return EventTime;
        }

        @Override
        public boolean Fire(Queue<MagEvent> magQueue) {
            boolean QueueFailed=false;
            if (MojePostava.AktMg>=MojePostava.MaxMg && MojePostava.AktMg-Cena< MojePostava.MaxMg){
                //Tímhle jsem přešel hranici regenerace. Tedy předtím neregeneroval. Dost možná jsem byl v overbuffu
                if (MojePostava.AktMg>MojePostava.MaxMg){
                    //Byl jsem. Už nebudu
                    stopOverbuffTimer();
                }
                if (MojePostava.RegenEnabled && MojePostava.GloomLevel==0){
                    //Jsem v realitě a můžu regenerovat
                    magQueue.add(new RegenTick(EventTime+MojePostava.DobaGloom[0]));
                    MojePostava.CDTStartTime=EventTime*1000L;
                }
            }
            if (!NastavMagyFastTime(MojePostava.AktMg-Cena, EventTime)){QueueFailed=true;}
            TS.ActivationTime+=TS.TimerLen;
            MagEvent New=new PeriodicCost(TS);
            magQueue.add(New);
            return (!QueueFailed);
        }
    }
    public class RegenTick implements MagEvent{
        long TickTime;
        public RegenTick(long TickTime){
            this.TickTime=TickTime;
        }

        @Override
        public long TimeOfEvent() {
            return TickTime;
        }

        @Override
        public boolean Fire(Queue<MagEvent> magQueue) {
            NastavMagyFastTime(Math.min(MojePostava.AktMg+MojePostava.RegenMg(),MojePostava.MaxMg),TickTime);
            if (MojePostava.AktMg<MojePostava.MaxMg){
                magQueue.add(new RegenTick(TickTime+MojePostava.DobaGloom[0]));
                MojePostava.CDTStartTime=TickTime*1000L;
            }
            return true;
        }
    }
    public class GloomTick implements MagEvent{
        long TickTime;
        public GloomTick(long TickTime){
            this.TickTime=TickTime;
        }

        @Override
        public long TimeOfEvent() {
            return TickTime;
        }

        @Override
        public boolean Fire(Queue<MagEvent> magQueue) {
            boolean QueueFailed=false;
            if (!NastavMagyFastTime((MojePostava.AktMg-MojePostava.CenaCurrentGloomFinal()),TickTime)){
                QueueFailed=true; //Šero tě vysálo!
            }
            //Nevysálo. Další interval začíná teď...
            MojePostava.CDTStartTime=TickTime*1000L;
            //A končí doufám za 15 minut nebo tek nějak
            magQueue.add(new GloomTick(TickTime+MojePostava.DobaGloom[MojePostava.GloomLevel]));
            return (!QueueFailed);
        }
    }
    public class OverBuffExpire implements MagEvent{
        long TickTime;
        public OverBuffExpire(long TickTime){
            this.TickTime=TickTime;
        }

        @Override
        public long TimeOfEvent() {
            return TickTime;
        }

        @Override
        public boolean Fire(Queue<MagEvent> magQueue) {
            NastavMagyFastTime(MojePostava.MaxMg, TickTime);
            MojePostava.PamatujSi("A během spánku mi vypršelo přebití.");
            return true;
        }
    }

    //QUICK SPELL SETTINGS

    AlertDialog QSdialog;
    public void QuickSpellDialog(){
        String[] Polozky = new String[MojePostava.Skilly.size()];
        for (int i=0;i<MojePostava.Skilly.size();i++)
        {
            Skill TheSkill =MojePostava.Skilly.get(i);
            //Elementální slevy
            int Cena=ZjistiRealCenu(TheSkill);
            Polozky[i]=MojePostava.Skilly.get(i).Jmeno+" ["+MojePostava.Skilly.get(i).Cena+"] ("+Cena+")";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ListView listview= new ListView(getApplicationContext());
        ListAdapter Adap = new ArrayAdapter<>(this,android.R.layout.simple_list_item_single_choice, Polozky);
        listview.setAdapter(Adap);
        listview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        builder.setTitle("Výběr rychlé Schopnosti");
        builder.setMessage("Která bude odteď rychlá schopnost?");
        builder.setView(listview);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                SelectQuickSkillPlacement(position);
                QSdialog.dismiss();
            }
        });
        QSdialog= builder.show();
        // Set up the buttons
    }
    int ChosenIndex=-1;
    AlertDialog QSPlaceDialog;
    public void SelectQuickSkillPlacement(final int i){
        Skill  TheSkill=MojePostava.Skilly.get(i);
        int Cena=ZjistiRealCenu(TheSkill);
        final String ButtonText=MojePostava.Skilly.get(i).Jmeno+" ( "+Cena+" )";
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Umístění");
        builder.setMessage("Které tlačítko pro rychlé schopnosti mám přepsat?");
        String[] QSNames = new String[6];
        for(int j=0;j<6;j++){
            if (MojePostava.QuickSpellNames[j].equals("")){
                QSNames[j]="Quick Spell "+(j+1);
            }else{
                QSNames[j]=MojePostava.QuickSpellNames[j];
            }
        }
        ListView QSView= new ListView(getApplicationContext());
        ListAdapter QSAdap = new ArrayAdapter<>(this,android.R.layout.simple_list_item_single_choice, QSNames);
        QSView.setAdapter(QSAdap);
        QSView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        ChosenIndex=-1; //Set this up. -1=No selection.
        QSView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                v.setSelected(true);
                ChosenIndex=position;
                MojePostava.QuickSpellNames[ChosenIndex] = TheSkill.Jmeno;
                if (findViewById(QSButtonID(ChosenIndex+1))!=null){ //Pokud by někdo vybíral QuickSpelly mimo hlavní obrazovku.
                    ((Button) findViewById(QSButtonID(ChosenIndex+1))).setText(ButtonText);
                }
                WriteToFile("Postava.txt", MojePostava.SaveSelfString());
                QSPlaceDialog.dismiss();
            }
        });
        builder.setView(QSView);
        QSPlaceDialog=builder.show();
    }

    public void SetGloomBiasDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Vychýlení");
        builder.setMessage("Je ti šero nakloněno? Skutečně? A jak moc?");
        // Set up the input
        final LinearLayout Layout = new LinearLayout(this);
        Layout.setOrientation(LinearLayout.VERTICAL);
        final EditText GloomPriceInput = new EditText(this);
        GloomPriceInput.setHint("Sleva na pobyt");
        final EditText GloomCastInput = new EditText(this);
        GloomCastInput.setHint("Sleva na kouzlení");
        Layout.addView(GloomPriceInput);
        Layout.addView(GloomCastInput);
        // Specify the type of input expected
        GloomCastInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        GloomPriceInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        GloomCastInput.setText("");
        builder.setView(Layout);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if ((GloomPriceInput.getText().toString().isEmpty()))
                {
                    GloomPriceInput.setText("0");
                }
                if ((GloomCastInput.getText().toString().isEmpty()))
                {
                    GloomCastInput.setText("0");
                }
                MojePostava.GloomBiasPrice=Integer.parseInt(GloomPriceInput.getText().toString());
                MojePostava.GloomBiasCast=Integer.parseInt(GloomCastInput.getText().toString());
            }
        });
        builder.setNegativeButton("Zrušit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}