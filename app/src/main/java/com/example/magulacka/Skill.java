package com.example.magulacka;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Skill{
    //Spelly&Skilly. NE SLEVY. NE PASIVKY.
    public String Jmeno; // Jméno musí být unikátní. Bude se podle něj přiřazovat.
    public int MinCena; //Rituály nesmí pod deset, Kouzla pod jedna, Schopnosti můžou být i zdarma. Ne, nejde to určit z Typu, páč tvorba baterky...
    public int Cena; //Základní. Bez všech slev!
    public int Typ; //0=schopnost, 1=spell, 2=Ritual, 3=Timer
    public MainActivity.skillTypes Type;
    public int Element; //1=Igni, Aqua, Terra, Aer, Nhi, 6=Phre, 0=Bez Elementu
    public Skill(){} //Prázdný konstruktor se hodí při vytváření. plný při načítání.
    public Skill(String Jmeno, int Cena, int Typ, int Element, int MinCena)
    {
        this.Jmeno=Jmeno;
        this.Cena=Cena;
        this.Typ=Typ;
        this.Element=Element;
        this.MinCena=MinCena;
    }
     public Element SaveSelfXML(Document SkillDoc){
         try{
             Element rootElement = SkillDoc.createElement("SKILL");
             SkillDoc.appendChild(rootElement);
             Element basicSkillDetails = SkillDoc.createElement("BASIC");
             basicSkillDetails.setAttribute("Name", Jmeno);
             basicSkillDetails.setAttribute("BasePrice", String.valueOf(Cena));
             basicSkillDetails.setAttribute("MinPrice", String.valueOf(MinCena));
             basicSkillDetails.setAttribute("Type", String.valueOf(Typ));
             basicSkillDetails.setAttribute("Element", String.valueOf(Element));
             rootElement.appendChild(basicSkillDetails);
             return rootElement;
         } catch (Exception e) {
             e.printStackTrace();
             return null;
         }
    }

}

