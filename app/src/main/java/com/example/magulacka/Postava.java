package com.example.magulacka;
import android.content.Context;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Postava {
    // Created by Stanlex on 6.1.2022
    public String Jmeno;
    public String Specializace;
    public int MaxHladina;
    public String[] QuickSpellNames;
    public int Kategorie;
    public long CDTStartTime; // Regenerace či šero. Hlavní timer. IN MILLIS
    public long OverbuffStartTime;  // IN MILLIS
    public int AktMg;
    public int MaxMg;
    public double OverbuffMultiplier;
    public int MaxOverbuff(){
        return (int)Math.round(MaxMg*OverbuffMultiplier);
    }
    public int RegenMg(){return CenaGloom[0];} //Protože kradu minulý kód. Pak to refaktoruj.
    public int OverbuffDoba;
    public boolean RegenEnabled;
    public int[] DobaGloom; // Countdown doby pro regeneraci[0] a i-té šero[i]
    public int[] CenaGloom; //Regenerace [0] a cena za i-té šero
    public int TypElementalSlev; //0=nic. 1=Spelly, 2=Rituály&Spelly
    public int GloomLevel;
    public int CenaCurrentGloomFinal(){
        if (GloomLevel==1){return CenaGloom1Final();}
        if (GloomLevel==2){return CenaGloom2Final();}
        return 0;
    }
    public int CenaGloom1Final(){return Math.max(CenaGloom[1]-GloomBiasPrice,1);} //Potřebuju zajistit, že až polezu do šera, nebude mě to ještě víc nabíjet.
    public int CenaGloom2Final(){if (MaxHladina>1){return Math.max(CenaGloom[2]-GloomBiasPrice,1);}else return 60;} //To samý, ale Šero nejspíš neusnadňuje pobyt těm, kdo se do dalších hladin nedostanou.
    public int[] SlevaSpelly; // Sleva za i-tou hladinu šera. SlevaSpelly[0]= sleva v realitě. Ta jako jediné se sčítá s ostatními.
    public int[] ElementSlevy; //1=Igni, Aqua, Terra, Aer, Nhi, 6=Phre, 0=Bez Elementu
    public int[] ElementSlevyRitual;
    public ArrayList<Skill> Skilly;
    public ArrayList<TimerSkill> Buffs; //Pointery na aktivní skilly a jen na aktivní skilly
    public int GloomBiasPrice=0; // Sleva na pobyt v šeru. Všechny hladiny
    public int GloomBiasCast=0; // Sleva na kouzlení v šeru. Všechny hladiny.
    public Postava(){
        DobaGloom = new int[4];
        CenaGloom=new int[4];
        SlevaSpelly=new int[4];
        ElementSlevy=new int[7];
        ElementSlevyRitual=new int[7];
        QuickSpellNames=new String[]{"","","","","",""};
        Skilly=new ArrayList<>();
        Buffs=new ArrayList<>();
        LogUdalosti=new LinkedList<>();
        //Zatím napevno. Příprava na případné rozšíření. 15min=900s
        DobaGloom[1]=900;
        DobaGloom[2]=900;
    }
    //Vrátí TRUE právě tehdy, když se úspěšně načte. FALSE v případe chyby.
    //V příapde chyby neudělá NIC a nové datové struktury zahodí.
    public boolean LoadSkillsString(String SkillString){
        ArrayList<Skill> NewList = new ArrayList<>();
        try {
            String Check = SkillString.substring(0, 8);
            String Template = "<Skilly>";
            if (!Check.equals(Template)) {
                return false;
            }
            SkillString = SkillString.substring(8); //
            String[] NacteneHodnoty = SkillString.split("@");
            if (!NacteneHodnoty[NacteneHodnoty.length - 1].equals("</Skilly>")) {
                return false;
            }
            int PocetSkillu = NacteneHodnoty.length - 1;
            for (int i = 0; i < PocetSkillu; i++) {
                String[] Sk = NacteneHodnoty[i].split("#");
                if (Sk[0].equals("TS")) { //Timed skill
                    TimerSkill TS = new TimerSkill(Sk[1], //Jmeno, Poznanka
                            Integer.parseInt(Sk[2]), Integer.parseInt(Sk[3]), //Cena,Typ
                            Integer.parseInt(Sk[4]), Integer.parseInt(Sk[5]), //Element, MinCena
                            Integer.parseInt(Sk[6]), Long.parseLong(Sk[7]), Double.parseDouble(Sk[9]), Long.parseLong(Sk[10])); // Effect, Length, intensity, Time
                    NewList.add(TS);
                } else { //[0] is "S", simple skill
                    Skill S = new Skill(Sk[1], //Jmeno
                            Integer.parseInt(Sk[2]), Integer.parseInt(Sk[3]), //Cena,Typ
                            Integer.parseInt(Sk[4]), Integer.parseInt(Sk[5])); //Element, MinCena
                    NewList.add(S);
                }
            }
        }
        catch (Exception e){
            return false;
        }
        //Doběhla nebezpečná část. Vypadá to, že to funguje.
        this.Skilly=NewList;
        return true;
    }
    public Skill ParseXMLSkill(Node SkillToparse){
        // Is it a Timer Skill?
        Skill NewSkill;
        int timerIndex=0;
        boolean isTimerSkill=false;
        for (int i=0; i<SkillToparse.getChildNodes().getLength(); i++){
            if (SkillToparse.getChildNodes().item(i).getNodeName().equals("TIMER")){isTimerSkill=true;timerIndex=i;break;}
        }
        //Create Regular Skill
        Node BasicAttributes = SkillToparse.getChildNodes().item(0);
        NewSkill =  new Skill(
                BasicAttributes.getAttributes().getNamedItem("Name").getNodeValue(),
                Integer.parseInt(BasicAttributes.getAttributes().getNamedItem("BasePrice").getNodeValue()),
                Integer.parseInt(BasicAttributes.getAttributes().getNamedItem("Type").getNodeValue()),
                Integer.parseInt(BasicAttributes.getAttributes().getNamedItem("Element").getNodeValue()),
                Integer.parseInt(BasicAttributes.getAttributes().getNamedItem("MinPrice").getNodeValue())
        );
        if (isTimerSkill){
            Node TimerAttributes = SkillToparse.getChildNodes().item(timerIndex); //Should be 1
            NewSkill = new TimerSkill(NewSkill,
                    Integer.parseInt(TimerAttributes.getAttributes().getNamedItem("Effect").getNodeValue()),
                    Long.parseLong(TimerAttributes.getAttributes().getNamedItem("Length").getNodeValue()),
                    Double.parseDouble(TimerAttributes.getAttributes().getNamedItem("Intensity").getNodeValue()),
                    Long.parseLong(TimerAttributes.getAttributes().getNamedItem("ActivationTime").getNodeValue())
                    );
        }
        return NewSkill;
    }
    public boolean LoadSkillsXML(String XMLSkillString){
        //TODO VALIDATE!!!
        Document LoadedSpellBook = MainActivity.convertStringToDocument(XMLSkillString);
        ArrayList<Skill> NewList = new ArrayList<>();
        NodeList LoadedSkills = LoadedSpellBook.getChildNodes().item(0).getChildNodes();
        for (int i = 0; i< LoadedSkills.getLength(); i++){
            NewList.add(ParseXMLSkill(LoadedSkills.item(i)));
        }
        this.Skilly=NewList;
        return true;
    }
    public String VypisSe(){
        String Vypis = "Jméno: "+Jmeno+System.getProperty("line.separator");
        Vypis += "Kategorie: "+Kategorie+System.getProperty("line.separator");
        Vypis += "Specializace: "+Specializace+System.getProperty("line.separator");
        Vypis += "Interval regenerace: " +CenaGloom[0]+" mg /"+DobaGloom[0]/60+" min"+System.getProperty("line.separator");
        Vypis += "Maximum energie: "+MaxMg+" mg . (Overbuff na "+OverbuffMultiplier+"-násobek)"+System.getProperty("line.separator");
        Vypis += "Max doba Overbuffu:"+OverbuffDoba/3600+"hod"+System.getProperty("line.separator");
        if (SlevaSpelly[0]>0){
            Vypis+="Sleva na Akční magii:"+SlevaSpelly[0]+" mg"+System.getProperty("line.separator");
        }
        Vypis+="Cena za 15min šera: "+CenaGloom[1]+"mg. Resp."+CenaGloom[2]+"mg. (2 hladina)"+System.getProperty("line.separator");
        Vypis+="Sleva v šeru: "+SlevaSpelly[1]+"mg. Resp."+SlevaSpelly[2]+"mg. (2 hladina)"+System.getProperty("line.separator");
        if (TypElementalSlev>0){Vypis+="Specialista na akční magii se slevami v oborech:"+System.getProperty("line.separator");}
        for (int i=1 ; i<7;i++){
            if (ElementSlevy[i]>0){
                Vypis+="Element "+MainActivity.SkillElements[i]+": "+ElementSlevy[i]+"mg."+System.getProperty("line.separator");
            }
        }
        if (TypElementalSlev==2){Vypis+="Specialista na rituální magii se slevami v oborech:"+System.getProperty("line.separator");}
        for (int i=1 ; i<7;i++){
            if (ElementSlevyRitual[i]>0){
                Vypis+="Element "+MainActivity.SkillElements[i]+": "+ElementSlevyRitual[i]+"mg."+System.getProperty("line.separator");
            }
        }
        if (GloomBiasPrice>0) {Vypis+="Šero usnadňuje pobyt:"+GloomBiasPrice+"mg."+System.getProperty("line.separator");}
        if (GloomBiasCast>0) {Vypis+="Šero usnadňuje kouzlení:"+GloomBiasCast+"mg.";}
        return Vypis;

    }
    public Document SaveSkillsXMLDoc(){
        Document SkillsDoc=null;
        try{
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        SkillsDoc = docBuilder.newDocument();
        Element SpellBook = SkillsDoc.createElement("SPELLBOOK");
        for (Skill S: Skilly ) {
            Element NewSkill = S.SaveSelfXML(SkillsDoc);
            SpellBook.appendChild(NewSkill);
        }
        SkillsDoc.appendChild(SpellBook);
        return SkillsDoc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public String SaveSkillsString(){
        String SavedSkillList="<Skilly>";
        for (Skill S: Skilly ) {
            if (S.getClass() == TimerSkill.class){
                TimerSkill T=(TimerSkill)S;
                SavedSkillList+="TS#"+T.Jmeno+"#"+T.Cena+"#"+T.Typ+"#"+T.Element+"#"+T.MinCena+"#"+T.TimerEffect+"#"+T.TimerLen+"#"+T.EffectIntensity+"#"+T.ActivationTime+"@";
            }else{
                SavedSkillList+="S#"+S.Jmeno+"#"+S.Cena+"#"+S.Typ+"#"+S.Element+"#"+S.MinCena+"@";
            }
        }
        SavedSkillList+="</Skilly>";
        return SavedSkillList;
    }
    public Document SaveSelfXMLDoc(){
        Document PostavaDoc=null;
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            PostavaDoc = docBuilder.newDocument();
            Element rootElement = PostavaDoc.createElement("POSTAVA");
            PostavaDoc.appendChild(rootElement);
            Element baseStatsElement = PostavaDoc.createElement("BASESTATS");
            baseStatsElement.setAttribute("Jmeno", Jmeno);
            baseStatsElement.setAttribute("Kategorie", String.valueOf(Kategorie));
            baseStatsElement.setAttribute("Specializace", Specializace);

            //Timers
            Element timeStampElement = PostavaDoc.createElement("TIMESTAMPS");
            timeStampElement.setAttribute("CDTStart", String.valueOf(CDTStartTime));
            timeStampElement.setAttribute("OverbuffStart", String.valueOf(OverbuffStartTime));
            rootElement.appendChild(timeStampElement);

            rootElement.appendChild(baseStatsElement);
            Element baseMagicElement = PostavaDoc.createElement("MAGIC");
            baseMagicElement.setAttribute("AktMg", String.valueOf(AktMg));
            baseMagicElement.setAttribute("MaxMg", String.valueOf(MaxMg));
            baseMagicElement.setAttribute("RegenEnabled", String.valueOf(RegenEnabled));
            baseMagicElement.setAttribute("OverbuffMultiplier", String.valueOf(OverbuffMultiplier));
            baseMagicElement.setAttribute("OverbuffLength", String.valueOf(OverbuffDoba));
            rootElement.appendChild(baseMagicElement);

            //Gloom
            Element GloomElement = PostavaDoc.createElement("GLOOMSTATS");
            GloomElement.setAttribute("MaxHladina",String.valueOf(MaxHladina));
            Element RealityElement = PostavaDoc.createElement("Reality");
            RealityElement.setAttribute("Price", String.valueOf(CenaGloom[0])); //Regenerace
            RealityElement.setAttribute("Discount", String.valueOf(SlevaSpelly[0]));
            RealityElement.setAttribute("Length", String.valueOf(DobaGloom[0])); //Regenerace
            GloomElement.appendChild(RealityElement);
            Element Gloomlevel1Element = PostavaDoc.createElement("Gloom1");
            Gloomlevel1Element.setAttribute("Price", String.valueOf(CenaGloom[1]));
            Gloomlevel1Element.setAttribute("Discount", String.valueOf(SlevaSpelly[1]));
            Gloomlevel1Element.setAttribute("Length", String.valueOf(DobaGloom[1]));
            GloomElement.appendChild(Gloomlevel1Element);
            Element Gloomlevel2Element = PostavaDoc.createElement("Gloom2");
            Gloomlevel2Element.setAttribute("Price", String.valueOf(CenaGloom[2]));
            Gloomlevel2Element.setAttribute("Discount", String.valueOf(SlevaSpelly[2]));
            Gloomlevel2Element.setAttribute("Length", String.valueOf(DobaGloom[2]));
            GloomElement.appendChild(Gloomlevel2Element);
            rootElement.appendChild(GloomElement);
            //Slevy na elementy
            Element ElementalActionDiscountsElement = PostavaDoc.createElement("ACTION_ELE_DISCOUTNS");
            for (int i = 0; i<7;i++){
                if (ElementSlevy[i]>0){
                    Element newElement = PostavaDoc.createElement(MainActivity.SkillElements[i]);
                    newElement.setAttribute("Value",String.valueOf(ElementSlevy[i]));
                    ElementalActionDiscountsElement.appendChild(newElement);
                }
            }
            rootElement.appendChild(ElementalActionDiscountsElement);
            Element ElementalRitualDiscountsElement = PostavaDoc.createElement("RITUAL_ELE_DISCOUNTS");
            for (int i = 0; i<7;i++){
                if (ElementSlevy[i]>0){
                    Element newElement = PostavaDoc.createElement(MainActivity.SkillElements[i]);
                    newElement.setAttribute("Value",String.valueOf(ElementSlevyRitual[i]));
                    ElementalRitualDiscountsElement.appendChild(newElement);
                }
            }
            rootElement.appendChild(ElementalRitualDiscountsElement);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PostavaDoc;
    }
    public String SaveSelfString(){
        String SavedChar="<Postava>";
        SavedChar+=Jmeno+"#"+Specializace+"#"+Kategorie+"#";
        SavedChar+= CDTStartTime +"#"+OverbuffStartTime+"#";
        SavedChar+=AktMg+"#"+MaxMg+"#"+OverbuffMultiplier+"#"+RegenEnabled+"#";
        SavedChar+=OverbuffDoba+"#"+GloomLevel+"#"+CenaGloom[0]+"#"+CenaGloom[1]+"#"+CenaGloom[2]+"#";
        SavedChar+=DobaGloom[0]+"#"+DobaGloom[1]+"#"+DobaGloom[2]+"#";
        SavedChar+=TypElementalSlev+"#"+SlevaSpelly[0]+"#"+SlevaSpelly[1]+"#"+SlevaSpelly[2];
        for (int i=1;i<7;i++) {
            SavedChar+="#"+ElementSlevy[i];
        }
        for (int i=0;i<6;i++) {
            SavedChar+="#"+ QuickSpellNames[i];
        }
        SavedChar+="#"+MaxHladina;
        SavedChar+="#"+GloomBiasPrice+"#"+GloomBiasCast;
        for (int i=1;i<7;i++) {
            SavedChar+="#"+ElementSlevyRitual[i];
        }
        SavedChar+="#</Postava>";

        return SavedChar;
    }

    public Boolean LoadSelfXML(String XMLString){
        //TODO VALIDATE!!!
        Document LoadedChar = MainActivity.convertStringToDocument(XMLString);
        Node BaseStats = LoadedChar.getElementsByTagName("BASESTATS").item(0);
        Jmeno = BaseStats.getAttributes().getNamedItem("Jmeno").getNodeValue();
        Kategorie = Integer.parseInt(BaseStats.getAttributes().getNamedItem("Kategorie").getNodeValue());
        Specializace = BaseStats.getAttributes().getNamedItem("Specializace").getNodeValue();
        Node Timestamps = LoadedChar.getElementsByTagName("TIMESTAMPS").item(0);
        CDTStartTime = Long.parseLong(Timestamps.getAttributes().getNamedItem("CDTStart").getNodeValue());
        OverbuffStartTime = Long.parseLong(Timestamps.getAttributes().getNamedItem("OverbuffStart").getNodeValue());
        Node baseMagic = LoadedChar.getElementsByTagName("MAGIC").item(0);
        AktMg = Integer.parseInt(baseMagic.getAttributes().getNamedItem("AktMg").getNodeValue());
        MaxMg = Integer.parseInt(baseMagic.getAttributes().getNamedItem("MaxMg").getNodeValue());
        RegenEnabled = Boolean.parseBoolean(baseMagic.getAttributes().getNamedItem("RegenEnabled").getNodeValue());
        OverbuffMultiplier = Double.parseDouble(baseMagic.getAttributes().getNamedItem("OverbuffMultiplier").getNodeValue());
        Node gloomStats = LoadedChar.getElementsByTagName("GLOOMSTATS").item(0);
        MaxHladina = Integer.parseInt(gloomStats.getAttributes().getNamedItem("MaxHladina").getNodeValue());

        NodeList GloomAdjustment = gloomStats.getChildNodes();
        for (int i = 0; i<3; i++){ //Assumes that the Gloom levels are saved IN SEQUENCE!!! Does NOT check names.
            Node currentLevelnode = GloomAdjustment.item(i);
            CenaGloom[i]=Integer.parseInt(currentLevelnode.getAttributes().getNamedItem("Price").getNodeValue());
            SlevaSpelly[i]=Integer.parseInt(currentLevelnode.getAttributes().getNamedItem("Discount").getNodeValue());
            DobaGloom[i]=Integer.parseInt(currentLevelnode.getAttributes().getNamedItem("Length").getNodeValue());
        }
        NodeList actionEleStats = LoadedChar.getElementsByTagName("ACTION_ELE_DISCOUTNS").item(0).getChildNodes();
        for (int i =0; i< actionEleStats.getLength();i++){
            Node oneStat = actionEleStats.item(i);
            for (int elenumber = 0; elenumber<7;elenumber++){
                if (MainActivity.SkillElements[elenumber].equals(oneStat.getNodeName())){
                    ElementSlevy[elenumber]=Integer.parseInt(oneStat.getAttributes().item(0).getNodeValue());
                    break;
                }
            }
        }
        NodeList ritualEleStats = LoadedChar.getElementsByTagName("RITUAL_ELE_DISCOUNTS").item(0).getChildNodes();
        for (int i =0; i< ritualEleStats.getLength();i++){
            Node oneStat = ritualEleStats.item(i);
            for (int elenumber = 0; elenumber<7;elenumber++){
                if (MainActivity.SkillElements[elenumber].equals(oneStat.getNodeName())){
                    ElementSlevyRitual[elenumber]=Integer.parseInt(oneStat.getAttributes().item(0).getNodeValue());
                    break;
                }
            }
        }
        //TODO OMG IT WORKS!!!
        return true;
    }
    public Boolean LoadSelfString(String CharString){
        //Trim = odstran whitespace na začátku a na konci. Kdyby nahodou
        String Check = CharString.trim().substring(0,9);
        String Template="<Postava>";
        if (!Check.equals(Template)){
            return false;
        }
        CharString=CharString.substring(9); //
        String[] NacteneHodnoty = CharString.split("#");
        //Trim = odstran whitespace na začátku a na konci. Kdyby nahodou
        if (!NacteneHodnoty[NacteneHodnoty.length-1].trim().equals("</Postava>")){
            return false;
        }
        //Ok then. We have work to do!
        Jmeno=NacteneHodnoty[0];
        Specializace=NacteneHodnoty[1];
        Kategorie=Integer.parseInt(NacteneHodnoty[2]);
        CDTStartTime =Long.parseLong(NacteneHodnoty[3]);
        OverbuffStartTime=Long.parseLong(NacteneHodnoty[4]);
        AktMg=Integer.parseInt(NacteneHodnoty[5]);
        MaxMg=Integer.parseInt(NacteneHodnoty[6]);
        OverbuffMultiplier=Double.parseDouble(NacteneHodnoty[7]);
        RegenEnabled=Boolean.parseBoolean(NacteneHodnoty[8]);
        OverbuffDoba=Integer.parseInt(NacteneHodnoty[9]);
        GloomLevel=Integer.parseInt(NacteneHodnoty[10]);
        for (int i=0; i<3;i++)
        {
            CenaGloom[i]=Integer.parseInt(NacteneHodnoty[i+11]);
        }
        for (int i=0; i<3;i++)
        {
            DobaGloom[i]=Integer.parseInt(NacteneHodnoty[i+14]);
        }
        TypElementalSlev=Integer.parseInt(NacteneHodnoty[17]);
        for (int i=0; i<3;i++)
        {
            SlevaSpelly[i]=Integer.parseInt(NacteneHodnoty[i+18]);
        }
        for (int i=0; i<6;i++)
        {
            ElementSlevy[i+1]=Integer.parseInt(NacteneHodnoty[i+21]);
            //Nultý element je bez elementu. To bude vždy nula
        }
        for (int i=0; i<6;i++)
        {
            QuickSpellNames[i]=NacteneHodnoty[i+27];
        }
        MaxHladina=Integer.parseInt(NacteneHodnoty[33]);
        GloomBiasPrice=Integer.parseInt(NacteneHodnoty[34]);
        GloomBiasCast=Integer.parseInt(NacteneHodnoty[35]);
        for (int i=0; i<6;i++)
        {
            ElementSlevyRitual[i+1]=Integer.parseInt(NacteneHodnoty[i+36]);
            //Nultý element je bez elementu. To bude vždy nula
        }
        return true;
    }

    //Zde začíná logování!
    int PametLogu = 200;
    public LinkedList<String> LogUdalosti;
    public void PamatujSi(String Log){
        if (LogUdalosti.size()>PametLogu){
            LogUdalosti.remove(); //Zapomeň první
        }
        LogUdalosti.add(MainActivity.TimeStampNow()+" "+Log);
    }
}
